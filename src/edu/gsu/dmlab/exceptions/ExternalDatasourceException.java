package edu.gsu.dmlab.exceptions;

public class ExternalDatasourceException extends RuntimeException {

	public ExternalDatasourceException(String message, Throwable cause) {
		super(message, cause);
	}

	public ExternalDatasourceException(String message) {
		super(message);
	}

	/**
	 * 
	 */
	private static final long serialVersionUID = 60399550131827095L;

}
