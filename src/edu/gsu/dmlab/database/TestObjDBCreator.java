package edu.gsu.dmlab.database;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import javax.sql.DataSource;

public class TestObjDBCreator {

	DataSource dsourc;
	String sourceLocation;
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd' 'HH:mm:ss", Locale.ENGLISH);

	public TestObjDBCreator(DataSource dsourc, String sourceLocation) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in TestObjDBCreator constructor.");
		this.dsourc = dsourc;
		this.sourceLocation = sourceLocation;
	}

	public boolean insertEvents() throws SQLException, IOException {
		Connection con = null;
		boolean done = false;

		String fileName = this.sourceLocation + File.separator + "oneyear_events.txt";

		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			String testExistsString = "SHOW TABLES LIKE 'testobjects';";
			PreparedStatement tableExistsPrepStmt = con.prepareStatement(testExistsString);
			ResultSet res = tableExistsPrepStmt.executeQuery();
			if (!res.next()) {
				String createquery = this.createString();
				tableExistsPrepStmt = con.prepareStatement(createquery);
				tableExistsPrepStmt.execute();
			}

			try (BufferedReader br = Files.newBufferedReader(Paths.get(fileName))) {
				String insertQuery = this.insertString();
				PreparedStatement stmt = con.prepareStatement(insertQuery);
				br.lines().skip(1).forEach(line -> {
					try {

						String[] splitLine = line.split("\t");
						String event_type = splitLine[2];
						stmt.setString(1, event_type);

						String event_starttime = splitLine[3];
						Date start = df.parse(event_starttime);
						stmt.setTimestamp(2, new Timestamp(start.getTime()));

						String location = splitLine[4];
						stmt.setString(3, location);

						String bbox = splitLine[5];
						stmt.setString(4, bbox);

						String ccode = splitLine[6];
						stmt.setString(5, ccode);

						int bboxSize = Integer.parseInt(splitLine[10]);
						stmt.setInt(6, bboxSize);

						stmt.addBatch();
					} catch (Exception e) {
						e.printStackTrace();
					}
				});

				int[] results = stmt.executeBatch();
				for (int resVal : results)
					if (resVal == 0)
						throw new SQLException("Insert event failed, no rows affected one one of the inserts.");
			}

		} finally {
			if (con != null) {
				con.close();
			}
		}

		return done;
	}

	private String createString() {
		StringBuilder sb = new StringBuilder();

		sb.append("CREATE TABLE `testobjects` ( ");
		sb.append("`eventID` integer unsigned NOT NULL AUTO_INCREMENT, ");
		sb.append("`eventType` varchar(255), ");
		sb.append("`startTime` timestamp, ");
		sb.append("`center` text, ");
		sb.append("`bbox` text, ");
		sb.append("`ccode` text, ");
		sb.append("`bboxArea` integer, ");
		sb.append("PRIMARY KEY (`eventID`), ");
		sb.append("KEY `startdate_obj_idx` (`startTime`) ");
		sb.append(")ENGINE=InnoDB AUTO_INCREMENT=0 DEFAULT CHARSET=utf8;");
		return sb.toString();
	}

	private String insertString() {
		StringBuilder sb = new StringBuilder();
		sb.append("INSERT INTO `testobjects` (");
		sb.append("`eventType`, ");
		sb.append("`startTime`, ");
		sb.append("`center`, ");
		sb.append("`bbox`, ");
		sb.append("`ccode`, ");
		sb.append("`bboxArea`) ");
		sb.append("VALUES(?,?,?,?,?,?);");
		return sb.toString();
	}
}
