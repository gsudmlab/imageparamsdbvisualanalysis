package edu.gsu.dmlab.database;

import java.awt.Rectangle;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.cs.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.cs.dmlab.util.Utility;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 */
public class ImageParamDBConnection2 implements IImageParamDBConnection {

	IImgParamNormalizer normalizer;
	DataSource dsourc = null;
	LoadingCache<CacheKey, double[][][]> cache = null;
	int paramDownSample = 64;
	int paramCells = 64;
	int paramDim = 10;
	String tableExtension;

	@SuppressWarnings("deprecation")
	HashFunction hashFunct = Hashing.md5();

	private class CacheKey {
		String key;
		IEvent event;
		Waveband wavelength;
		boolean leftSide;
		HashCode hc = null;

		/**
		 * Key object for the cache of image parameters for IEvent objects.
		 * 
		 * @param event
		 *            The IEvent object that the cached object is for.
		 * @param leftSide
		 *            Since these are used in tracking I want to know if we are coming
		 *            from the left hand side of a decision boundary meaning that we
		 *            should use the last valid time of the object otherwise it is the
		 *            first valid time.
		 * @param wavelength
		 *            The wavelength to pull and cache.
		 * @param hashFunct
		 *            Just the hash function to use to create an identifier.
		 */
		public CacheKey(IEvent event, boolean leftSide, Waveband wavelength, HashFunction hashFunct) {
			StringBuilder evntName = new StringBuilder();
			evntName.append(event.getUUID().toString());
			evntName.append(wavelength.toString());
			if (leftSide) {
				evntName.append("T");
			} else {
				evntName.append("F");
			}
			this.key = evntName.toString();
			this.hc = hashFunct.newHasher().putString(this.key, Charsets.UTF_8).hash();
			this.event = event;
			this.leftSide = leftSide;
			this.wavelength = wavelength;
		}

		@Override
		public void finalize() {
			this.hc = null;
			this.key = null;
			this.event = null;
		}

		public IEvent getEvent() {
			return this.event;
		}

		public boolean isLeftSide() {
			return this.leftSide;
		}

		public Waveband getWavelength() {
			return this.wavelength;
		}

		public String getKey() {
			return key;
		}

		@Override
		public int hashCode() {
			return hc.asInt();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof CacheKey) {
				CacheKey val = (CacheKey) obj;
				return val.getKey().compareTo(this.key) == 0;
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			return this.key;
		}
	}

	/**
	 * Constructor for the image parameter access class.
	 * 
	 * @param dsourc
	 *            The connection pool to the database that contains the data.
	 * @param normalizer
	 *            An image parameter normalizer object to do 0-1 normalization (Can
	 *            be null and won't try to normalize).
	 * @param paramDownSample
	 *            The divisor used to downsize the IEvent object points to fit
	 *            within the parameter space.
	 * @param paramDim
	 *            The number of image parameters stored at each x, y cell location.
	 * @param paramCells
	 *            The number of parameter cells wide and tall the parameter space
	 *            is.
	 * @param maxCacheSize
	 *            When accessing parameters for an IEvent, how many do we want to
	 *            cache in LRU queue for access again without having to hit the DB
	 *            again.
	 * @param tableExtension
	 *            The extension to the parameter table for the particular
	 *            experimental set of parameters you want to access.
	 */
	public ImageParamDBConnection2(DataSource dsourc, IImgParamNormalizer normalizer, int paramDownSample, int paramDim,
			int paramCells, int maxCacheSize, String tableExtension) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in ImageDBConnection constructor.");

		this.paramDownSample = paramDownSample;
		this.paramCells = paramCells;
		this.paramDim = paramDim;
		this.dsourc = dsourc;
		this.normalizer = normalizer;
		this.tableExtension = tableExtension;

		this.cache = CacheBuilder.newBuilder().maximumSize(maxCacheSize)
				.build(new CacheLoader<CacheKey, double[][][]>() {
					public double[][][] load(CacheKey key) {
						return fetchImageParams(key);
					}
				});
	}

	/**
	 * Gets the image parameters for the given wavelength that intersect the MBR of
	 * the IEvent that is passed in. The parameters are in the form [x][y][paramId].
	 * This access is cached for future fast retrieval if not pushed out of the LRU
	 * cache.
	 * 
	 * @param event
	 *            The event that we want the image parameters for.
	 * @param wavelength
	 *            The wavelength of image that we want the parameters of.
	 * @param leftSide
	 *            If left side, we use the end time of the IEvent, if not we use the
	 *            begin time.
	 * @return The image parameters that intersect the MBR.
	 */
	public double[][][] getImageParamForWave(IEvent event, Waveband wavelength, boolean leftSide) {
		CacheKey key = new CacheKey(event, leftSide, wavelength, this.hashFunct);
		double[][][] returnValue = this.cache.getUnchecked(key);
		key = null;
		return returnValue;
	}

	/**
	 * Gets the image parameters for the given image parameter wavelength pair array
	 * that intersect the MBR of the IEvent passed in. The parameters are in the
	 * form that each matrix represents a particular image parameter at a given
	 * wavelength that corresponds to the position they were in on the params array.
	 * 
	 * @param event
	 *            The event that we want the image parameters for.
	 * @param params
	 *            The array of specific parameter/wavelength pairs that we wish to
	 *            get.
	 * @param leftSide
	 *            If left side, we use the end time of the IEvent, if not we use the
	 *            begin time.
	 * @return The image parameters that intersect the MBR.
	 */
	public DenseMatrix[] getImageParamForEv(IEvent event, ImageDBWaveParamPair[] params, boolean leftSide) {
		// Get the image parameters for each wavelength in the set of dimensions
		DenseMatrix[] dimMatArr = new DenseMatrix[params.length];
		for (int i = 0; i < params.length; i++) {

			double[][][] paramVals = this.getImageParamForWave(event, params[i].wavelength, leftSide);
			int rows = paramVals.length;
			int cols = paramVals[0].length;

			// get the param indicated by the dims array at depth i
			// the param value is from 1 to 10 where array index is 0-9
			// hence the -1
			int paramIdx = params[i].parameter - 1;
			double[][] data = new double[rows][cols];
			for (int x = 0; x < cols; x++) {
				for (int y = 0; y < rows; y++) {
					data[y][x] = paramVals[y][x][paramIdx]; 
				}
			}
			dimMatArr[i] = new JMatrix(data);
		}
		return dimMatArr;
	}

	/**
	 * Gets the image ids from the month that the input period begins through either
	 * the end of the period or the end of the month, which ever comes first. The
	 * ids will be for the input wavelength of images only.
	 * 
	 * @param period
	 *            The period over which we wish to get Ids for.
	 * @param wavelength
	 *            The wavelength of images we wish to get Ids for.
	 * @return The Date and Id pairs of all the images in the range and of the input
	 *         wavelength.
	 */
	public ImageDBDateIdPair[] getImageIdsForInterval(Interval period, Waveband wavelength) {
		ImageDBDateIdPair[] result = null;
		try {
			Connection con = null;
			try {
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);

				DateTime startDateTime = period.getStart();
				DateTime endDateTime = period.getEnd();

				ArrayList<ImageDBDateIdPair> idList = new ArrayList<ImageDBDateIdPair>();

				boolean runAgain = false;
				do {
					Interval queryPeriod = new Interval(startDateTime, endDateTime);
					String queryFileString = buildFileQueryString(queryPeriod);
					Timestamp startTime = new Timestamp(queryPeriod.getStartMillis());
					Timestamp endTime = new Timestamp(queryPeriod.getEndMillis());

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(wavelength));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						Timestamp ts = imgFileIdResults.getTimestamp(2);
						ImageDBDateIdPair pair = new ImageDBDateIdPair();
						pair.id = id;
						pair.period = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
						idList.add(pair);
					}
					imgFileIdResults.close();

					if (startDateTime.getMonthOfYear() == 12) {
						startDateTime = new DateTime(startDateTime.getYear() + 1, 1, 1, 0, 0, 0);
					} else {
						startDateTime = new DateTime(startDateTime.getYear(), startDateTime.getMonthOfYear() + 1, 1, 0,
								0, 0);
					}

					if (startDateTime.getYear() < endDateTime.getYear()) {
						runAgain = true;
					} else if (startDateTime.getYear() == endDateTime.getYear()) {
						if (startDateTime.getMonthOfYear() <= endDateTime.getMonthOfYear()) {
							runAgain = true;
						} else {
							runAgain = false;
						}
					} else {
						runAgain = false;
					}
				} while (runAgain);
				result = new ImageDBDateIdPair[idList.size()];
				for (int i = 0; i < result.length; i++)
					result[i] = idList.get(i);
			} catch (SQLException ex) {
				ex.printStackTrace();
			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

	/**
	 * Gets the full disk set of image parameters for all image parameters of the
	 * image with the given id in the month that the given input period starts.
	 * <br>
	 * For instance, suppose we have 28 different settings, hence 28 columns in our
	 * image_param_xxxxxx_B table. For id = 435 and period p, it looks at the table 
	 * 'image_param_[p.year][p.month]_B' and queries all the rows with id 435.
	 * It then converts the results into an array of matrices each of which represent
	 * one single 64X64 image parameter.
	 * 
	 * @param period
	 *            The month in which we wish to get the parameters for.
	 * @param id
	 *            The id of the image in the given month.
	 * @return The set of full disk parameters for the image with the given id.
	 */
	public DenseMatrix[] getImageParamForId(Interval period, int id) {
		String queryParamString = this.buildQueryParamString(period);
//		DenseMatrix[] dimMatArr = new DenseMatrix[10];
		DenseMatrix[] dimMatArr = new DenseMatrix[this.paramDim];
		try {
			Connection con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			try {
				PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);

				param_prep_stmt.setInt(1, id);
				param_prep_stmt.setInt(2, (int) 1);
				param_prep_stmt.setInt(3, (int) this.paramCells);
				param_prep_stmt.setInt(4, (int) 1);
				param_prep_stmt.setInt(5, (int) this.paramCells);

				ResultSet imgParamRslts = param_prep_stmt.executeQuery();

				double[][][] retVal = new double[this.paramCells][this.paramCells][this.paramDim];
				while (imgParamRslts.next()) {
					int x = imgParamRslts.getInt("x");

					int y = imgParamRslts.getInt("y");

					for (int i = 1; i < this.paramDim + 1; i++) {
						retVal[y - 1][x - 1][i - 1] = imgParamRslts.getFloat("p" + i);
					}
				}

				if (this.normalizer != null)
					this.normalizer.normalizeParameterValues(retVal);
				int rows = retVal[0].length;
				int cols = retVal.length;

				for (int paramIdx = 0; paramIdx < this.paramDim; paramIdx++) {
					double[][] data = new double[rows][cols];
					for (int x = 0; x < cols; x++) {
						for (int y = 0; y < rows; y++) {
							data[y][x] = retVal[y][x][paramIdx];
						}
					}
					dimMatArr[paramIdx] = new JMatrix(data);
				}

			} catch (SQLException ex) {
				ex.printStackTrace();

			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return dimMatArr;
	}

	/**
	 * Gets the header information for the image in the month that the period begins
	 * that has the passed in id value.
	 * 
	 * @param period
	 *            The month in which to get the image header.
	 * @param id
	 *            The id associated with the image header we wish to get.
	 * @return The header for the image in the month requested.
	 * @throws SQLException
	 *             When something with the serve failed.
	 */
	public ImageDBFitsHeaderData getHeaderForId(Interval period, int id) {
		Connection con = null;
		ImageDBFitsHeaderData headerData = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();

			String queryString = "SELECT x0, y0, rSun, dSun, cdelt FROM image_header_" + startTime.getYear();
			if ((startTime.getMonthOfYear()) < 10) {
				queryString += "0" + (startTime.getMonthOfYear());
			} else {
				queryString += "" + (startTime.getMonthOfYear());
			}
			queryString += " WHERE file_id = ? ;";
			PreparedStatement hdr_prep_stmt = con.prepareStatement(queryString);
			hdr_prep_stmt.setInt(1, id);

			ResultSet hdrRslts = hdr_prep_stmt.executeQuery();

			if (hdrRslts.next()) {
				headerData = new ImageDBFitsHeaderData();
				headerData.X0 = hdrRslts.getDouble(1);
				headerData.Y0 = hdrRslts.getDouble(2);
				headerData.R_SUN = hdrRslts.getDouble(3);
				headerData.DSUN = hdrRslts.getDouble(4);
				headerData.CDELT = hdrRslts.getDouble(5);
			}
			hdrRslts.close();
			hdr_prep_stmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return headerData;
	}

	/*
	 * Private method area, here be dragons.
	 */

	/**
	 * This is called directly by the cache. The minimum size of bounding results
	 * returned is 5X5 cell values.
	 * 
	 * @param key
	 * @return
	 */
	private double[][][] fetchImageParams(CacheKey key) {
		double[][][] retVal = null;

		String queryFileString = this.buildFileQueryString(key);
		String queryParamString = this.buildQueryParamString(key);

		Timestamp startTime = new Timestamp(key.getEvent().getTimePeriod().getStartMillis());
		Timestamp endTime = new Timestamp(key.getEvent().getTimePeriod().getEndMillis());

		Rectangle tmpBbox = key.getEvent().getBBox();
		int wSize;
		int xStart;
		if ((tmpBbox.width / this.paramDownSample) < 5) {
			xStart = (tmpBbox.x / this.paramDownSample) - 2;
			wSize = 5;
		} else {
			xStart = (tmpBbox.x / this.paramDownSample);
			wSize = (tmpBbox.width / this.paramDownSample);
		}

		int hSize;
		int yStart;
		if ((tmpBbox.height / this.paramDownSample) < 5) {
			yStart = (tmpBbox.y / this.paramDownSample) - 2;
			hSize = 5;
		} else {
			yStart = (tmpBbox.y / this.paramDownSample);
			hSize = (tmpBbox.height / this.paramDownSample);
		}
		Rectangle rec = new Rectangle(xStart, yStart, wSize, hSize);

		retVal = new double[rec.height][rec.width][this.paramDim];

		int tryCount = 0;
		boolean executed = false;
		while (!executed && tryCount < 3) {
			try {
				Connection con = null;
				try {
					con = this.dsourc.getConnection();
					con.setAutoCommit(true);

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(key.getWavelength()));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					ArrayList<Integer> idList = new ArrayList<Integer>();
					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						idList.add(Integer.valueOf(id));
					}

					if (idList.isEmpty())
						System.out.println("No Img In DB: " + key.event.getTimePeriod().getStart());

					boolean gotParams = false;
					while (!gotParams && !idList.isEmpty()) {
						try {
							PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);
							
							/* TODO:
							 * The code below is changed on 2019-01-18 due to the fact that
							 * x and y values are populated to the database (images_production_db)
							 * in reverse.
							 * This change is a fix for that mistake temporarily.*/
							
							
							// Original X and Y 
							int id = idList.get(0);
							idList.remove(0);
							param_prep_stmt.setInt(1, id);
							param_prep_stmt.setInt(2, (int) rec.x);
							param_prep_stmt.setInt(3, (int) rec.x + (int) rec.width - 1);
							param_prep_stmt.setInt(4, (int) rec.y);
							param_prep_stmt.setInt(5, (int) rec.y + (int) rec.height - 1);

							ResultSet imgParamRslts = param_prep_stmt.executeQuery();
							int count = 0;
							while (imgParamRslts.next()) {
								int x = imgParamRslts.getInt("x");
								x = x - (int) rec.x;

								int y = imgParamRslts.getInt("y");
								y = y - (int) rec.y;

								for (int i = 1; i < this.paramDim + 1; i++) {
									double val = imgParamRslts.getFloat("p" + i);
									retVal[y][x][i - 1] = val;
								}
								count++;
							}
							
							
							/*
							// Reversed as a temporary fix
							int id = idList.get(0);
							idList.remove(0);
							param_prep_stmt.setInt(1, id);
							param_prep_stmt.setInt(4, (int) rec.x);
							param_prep_stmt.setInt(5, (int) rec.x + (int) rec.width - 1);
							param_prep_stmt.setInt(2, (int) rec.y);
							param_prep_stmt.setInt(3, (int) rec.y + (int) rec.height - 1);

							ResultSet imgParamRslts = param_prep_stmt.executeQuery();
							int count = 0;
							while (imgParamRslts.next()) {
								int x = imgParamRslts.getInt("y");
								x = x - (int) rec.x;

								int y = imgParamRslts.getInt("x");
								y = y - (int) rec.y;

								for (int i = 1; i < this.paramDim + 1; i++) {
									double val = imgParamRslts.getFloat("p" + i);
									retVal[y][x][i - 1] = val;
								}
								count++;
							}
							*/

							if (count == (int) rec.height * (int) rec.width) {
								executed = true;
								gotParams = true;
							}
						} catch (SQLException ex) {
							ex.printStackTrace();
						}
					}

				} catch (SQLException ex) {
					ex.printStackTrace();
				} finally {
					if (con != null) {
						con.close();
					}
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			tryCount++;
		}

		if (this.normalizer != null)
			this.normalizer.normalizeParameterValues(retVal);
		return retVal;
	}

	private String buildFileQueryString(CacheKey key) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = key.getEvent().getTimePeriod().getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		if (key.isLeftSide()) {
			queryString.append(" DESC;");
		} else {
			queryString.append(" ASC;");
		}
		return queryString.toString();
	}

	private String buildFileQueryString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id, startdate FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		queryString.append(" DESC;");
		return queryString.toString();
	}

	private String buildQueryParamString(CacheKey key) {
		// for constructing the table names from the year and month of
		// the event

		Interval period = key.getEvent().getTimePeriod();
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT x, y, ");
		for (int i = 0; i < this.paramDim - 1; i++) {
			queryString.append("p" + (i + 1) + ", ");
		}
		queryString.append("p" + (this.paramDim) + " ");
		queryString.append("FROM image_params_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(this.tableExtension);
		queryString.append(" WHERE ( file_id = ? ) AND (x BETWEEN ? AND ? ) AND (y BETWEEN ? AND ?);");
		return queryString.toString();
	}

	private String buildQueryParamString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT x, y, ");
		for (int i = 0; i < this.paramDim - 1; i++) {
			queryString.append("p" + (i + 1) + ", ");
		}
		queryString.append("p" + (this.paramDim) + " ");
		queryString.append("FROM image_params_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
//		queryString.append(this.tableExtension);
		queryString.append(" WHERE ( file_id = ? ) AND (x BETWEEN ? AND ? ) AND (y BETWEEN ? AND ?);");
		return queryString.toString();
	}
}