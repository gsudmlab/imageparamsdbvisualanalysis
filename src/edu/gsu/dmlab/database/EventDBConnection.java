package edu.gsu.dmlab.database;

import java.awt.Polygon;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.Interval;

import edu.gsu.dmlab.database.interfaces.IEventDBConnection;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.GenericEvent;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.cs.dmlab.geometry.Point2D;

/**
 * This is implemented to retrieve all information about each events (AR, CH, QS)
 * from testobjects in testobj_db.
 *
 */
public class EventDBConnection implements IEventDBConnection {

	DataSource dsourc = null;

	public EventDBConnection(DataSource dsourc) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in EventDBConnection constructor.");
		this.dsourc = dsourc;
	}

	@Override
	public List<IEvent> getAllEvents(EventType type) {
		ArrayList<IEvent> results = new ArrayList<IEvent>();
		try {
			Connection con = null;
			String queryString = "SELECT eventID, startTime, center, ccode, bbox FROM testobjects";
			
			// 12 months
			queryString += " where ((startTime between '2012-01-01 00:00:00' and '2012-12-31 23:59:59') and eventType = '"
					+ this.getTypeString(type) + "');";
			// 1 month
//			queryString += " where ((startTime between '2012-01-01 00:00:00' and '2012-01-31 23:59:59') and eventType = '"
//					+ this.getTypeString(type) + "');";

			try {
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);

				PreparedStatement evnts_prep_stmt = con.prepareStatement(queryString);
				ResultSet rs = evnts_prep_stmt.executeQuery();

				while (rs.next()) {
					int id = rs.getInt(1);
					Timestamp startTime = rs.getTimestamp(2);
					String centerString = rs.getString(3);
					String shapeString = rs.getString(4);
					String bboxString = rs.getString(5);

					Point2D center = this.getPoint(this.removeBrackets(centerString));
					try {
						Polygon poly;
						if (shapeString.isEmpty()) {
							poly = this.getPoly(bboxString);
						} else {
							poly = this.getPoly(shapeString);
						}

						Interval range = new Interval(startTime.getTime(), startTime.getTime() + (60 * 60 * 4 * 1000));

						IEvent ev = new GenericEvent(id, range, center, poly.getBounds(), poly, type);
						results.add(ev);
					} catch (Exception ex) {
						ex.printStackTrace();
					}
				}

			} catch (SQLException ex) {
				ex.printStackTrace();
			} finally {
				if (con != null) {
					con.close();
				}
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return results;
	}

	private String getTypeString(EventType type) {

		switch (type) {
		case ACTIVE_REGION:
			return "AR";
		case CORONAL_HOLE:
			return "CH";
		case QUIET_SUN:
			return "QS";
		default:
			return "QS";
		}

	}

	/**
	 * getPoly: returns an objectList of 2D points extracted from the input
	 * string the objectList of points are assumed to create a polygon, they are
	 * not tested
	 * 
	 * @param pointsString
	 *            :the string to extract the points from
	 * @return :returns the objectList of 2D points
	 */
	private Polygon getPoly(String pointsString) {
		pointsString = this.removeBrackets(pointsString);
		String[] pointsStrings = pointsString.split(",");
		String xy;
		ArrayList<Integer> xPoints = new ArrayList<Integer>();
		ArrayList<Integer> yPoints = new ArrayList<Integer>();
		for (int i = 0; i < pointsStrings.length; i++) {
			xy = pointsStrings[i];
			Point2D pnt = this.getPoint(xy);
			xPoints.add((int) pnt.x);
			yPoints.add((int) pnt.y);
		}
		int[] xArr = new int[xPoints.size()];
		int[] yArr = new int[yPoints.size()];
		for (int i = 0; i < xArr.length; i++) {
			xArr[i] = xPoints.get(i);
			yArr[i] = yPoints.get(i);
		}

		Polygon poly = new Polygon(xArr, yArr, xArr.length);
		return poly;
	}

	/**
	 * getPoint :returns a 2D point from the input string
	 * 
	 * @param xy
	 *            :input string containing x and y coordinate
	 * @return :the 2D point extracted from the string
	 */
	private Point2D getPoint(String xy) {
		int spaceIdx = xy.indexOf(' ');
		double x = Double.parseDouble(xy.substring(0, spaceIdx));
		double y = Double.parseDouble(xy.substring(spaceIdx));
		return new Point2D(x, y);
	}

	/**
	 * removeBrackets : used to remove anything preceeding or following a ( or a
	 * )
	 * 
	 * @param in
	 *            : the string to trim up
	 * @return : the trimmed string
	 */
	private String removeBrackets(String in) {
		int begin = in.indexOf('(');
		int end = in.lastIndexOf(')');
		while (begin >= 0 || end >= 0) {
			in = in.substring(begin + 1, end);
			begin = in.indexOf('(');
			end = in.lastIndexOf(')');
		}
		return in;
	}
}
