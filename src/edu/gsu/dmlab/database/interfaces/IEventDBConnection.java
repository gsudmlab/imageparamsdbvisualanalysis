package edu.gsu.dmlab.database.interfaces;

import java.util.List;

import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;

public interface IEventDBConnection {
	public List<IEvent> getAllEvents(EventType type);
}
