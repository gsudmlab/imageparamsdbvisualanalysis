package edu.gsu.dmlab.database.interfaces;

import org.joda.time.Interval;

import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import smile.math.matrix.DenseMatrix;

public interface IImageParamDBConnection {

	/**
	 * Gets the image parameters for the given wavelength that intersect the MBR of
	 * the IEvent that is passed in. The parameters are in the form [x][y][paramId].
	 * This access is cached for future fast retrieval if not pushed out of the LRU
	 * cache.
	 * 
	 * @param event
	 *            The event that we want the image parameters for.
	 * @param wavelength
	 *            The wavelength of image that we want the parameters of.
	 * @param leftSide
	 *            If left side, we use the end time of the IEvent, if not we use the
	 *            begin time.
	 * @return The image parameters that intersect the MBR.
	 */
	public double[][][] getImageParamForWave(IEvent event, Waveband wavelength, boolean leftSide);

	/**
	 * Gets the image parameters for the given image parameter wavelength pair array
	 * that intersect the MBR of the IEvent passed in. The parameters are in the
	 * from that each matrix represents a particular image parameter at a given
	 * wavelength that corresponds to the position they were in on the params array.
	 * 
	 * @param event
	 *            The event that we want the image parameters for.
	 * @param params
	 *            The array of specific parameter/wavelength pairs that we wish to
	 *            get.
	 * @param leftSide
	 *            If left side, we use the end time of the IEvent, if not we use the
	 *            begin time.
	 * @return The image parameters that intersect the MBR.
	 */
	public DenseMatrix[] getImageParamForEv(IEvent event, ImageDBWaveParamPair[] params, boolean leftSide);

	/**
	 * Gets the image ids from the month that the input period begins through either
	 * the end of the period or the end of the month, which ever comes first. The
	 * ids will be for the input wavelength of images only.
	 * 
	 * @param period
	 *            The period over which we wish to get Ids for.
	 * @param wavelength
	 *            The wavelength of images we wish to get Ids for.
	 * @return The Date and Id pairs of all the images in the range and of the input
	 *         wavelength.
	 */
	public ImageDBDateIdPair[] getImageIdsForInterval(Interval period, Waveband wavelength);

	/**
	 * Gets the full disk set of image parameters for all image parameters of the
	 * image with the given id in the month that the given input period starts.
	 * 
	 * @param period
	 *            The month in which we wish to get the parameters for.
	 * @param id
	 *            The id of the image in the given month.
	 * @return The set of full disk parameters for the image with the given id.
	 */
	public DenseMatrix[] getImageParamForId(Interval period, int id);

	/**
	 * Gets the header information for the image in the month that the period begins
	 * that has the passed in id value.
	 * 
	 * @param period
	 *            The month in which to get the image header.
	 * @param id
	 *            The id associated with the image header we wish to get.
	 * @return The header for the image in the month requested.
	 * 
	 */
	public ImageDBFitsHeaderData getHeaderForId(Interval period, int id);

}
