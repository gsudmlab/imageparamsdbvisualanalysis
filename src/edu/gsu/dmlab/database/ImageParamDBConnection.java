package edu.gsu.dmlab.database;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

import javax.imageio.ImageIO;
import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.HashCode;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.cs.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.cs.dmlab.util.Utility;
import smile.math.matrix.DenseMatrix;
import smile.math.matrix.JMatrix;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 * @version {@value edu.gsu.dmlab.ConstValues#major_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#minor_ver}.
 *          {@value edu.gsu.dmlab.ConstValues#build_ver}
 */
public class ImageParamDBConnection implements IImageParamDBConnection {
	IImgParamNormalizer normalizer;
	DataSource dsourc = null;
	// IBLASDataTypeFactory factory;
	LoadingCache<CacheKey, double[][][]> cache = null;
	int paramDownSample = 64;
	int paramDim = 10;
	int vectRows = 256;
	HashFunction hashFunct = Hashing.md5();

	private class CacheKey {
		String key;
		IEvent event;
		Waveband wavelength;
		boolean leftSide;
		HashCode hc = null;

		public CacheKey(IEvent event, boolean leftSide, Waveband wavelength, HashFunction hashFunct) {
			StringBuilder evntName = new StringBuilder();
			evntName.append(event.getUUID().toString());
			evntName.append(wavelength.toString());
			if (leftSide) {
				evntName.append("T");
			} else {
				evntName.append("F");
			}
			this.key = evntName.toString();
			this.hc = hashFunct.newHasher().putString(this.key, Charsets.UTF_8).hash();
			this.event = event;
			this.leftSide = leftSide;
			this.wavelength = wavelength;
		}

		@Override
		public void finalize() {
			this.hc = null;
			this.key = null;
			this.event = null;
		}

		public IEvent getEvent() {
			return this.event;
		}

		public boolean isLeftSide() {
			return this.leftSide;
		}

		public Waveband getWavelength() {
			return this.wavelength;
		}

		public String getKey() {
			return key;
		}

		@Override
		public int hashCode() {
			return hc.asInt();
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof CacheKey) {
				CacheKey val = (CacheKey) obj;
				return val.getKey().compareTo(this.key) == 0;
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			return this.key;
		}
	}

	public ImageParamDBConnection(DataSource dsourc, IImgParamNormalizer normalizer, int maxCacheSize) {
		if (dsourc == null)
			throw new IllegalArgumentException("DataSource cannot be null in ImageDBConnection constructor.");
		// if (normalizer == null)
		// throw new IllegalArgumentException("Param Normalizer cannot be null
		// in ImageDBConnection constructor.");

		this.dsourc = dsourc;
		this.normalizer = normalizer;

		this.cache = CacheBuilder.newBuilder().maximumSize(maxCacheSize)
				.build(new CacheLoader<CacheKey, double[][][]>() {
					public double[][][] load(CacheKey key) {
						return fetchImageParams(key);
					}
				});
	}

	@Override
	public double[][][] getImageParamForWave(IEvent event, Waveband wavelength, boolean leftSide) {
		CacheKey key = new CacheKey(event, leftSide, wavelength, this.hashFunct);
		double[][][] returnValue = this.cache.getUnchecked(key);
		key = null;
		return returnValue;
	}

	private double[][][] fetchImageParams(CacheKey key) {
		double[][][] retVal = null;

		String queryFileString = this.buildFileQueryString(key);
		String queryParamString = this.buildQueryParamString(key);

		Timestamp startTime = new Timestamp(key.getEvent().getTimePeriod().getStartMillis());
		Timestamp endTime = new Timestamp(key.getEvent().getTimePeriod().getEndMillis());

		Rectangle tmpBbox = key.getEvent().getBBox();
		int wSize;
		int xStart;
		if ((tmpBbox.width / this.paramDownSample) < 5) {
			xStart = (tmpBbox.x / this.paramDownSample) - 2;
			wSize = 5;
		} else {
			xStart = (tmpBbox.x / this.paramDownSample);
			wSize = (tmpBbox.width / this.paramDownSample);
		}

		int hSize;
		int yStart;
		if ((tmpBbox.height / this.paramDownSample) < 5) {
			yStart = (tmpBbox.y / this.paramDownSample) - 2;
			hSize = 5;
		} else {
			yStart = (tmpBbox.y / this.paramDownSample);
			hSize = (tmpBbox.height / this.paramDownSample);
		}
		Rectangle rec = new Rectangle(xStart, yStart, wSize, hSize);

		retVal = new double[rec.height][rec.width][this.paramDim];

		int tryCount = 0;
		boolean executed = false;
		while (!executed && tryCount < 3) {
			try {
				Connection con = null;
				try {
					con = this.dsourc.getConnection();
					con.setAutoCommit(true);

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(key.getWavelength()));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					ArrayList<Integer> idList = new ArrayList<Integer>();
					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						idList.add(Integer.valueOf(id));
					}

					boolean gotParams = false;
					while (!gotParams && !idList.isEmpty()) {
						try {
							PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);
							int id = idList.get(0);
							idList.remove(0);
							param_prep_stmt.setInt(1, id);
							param_prep_stmt.setInt(2, (int) rec.y);
							param_prep_stmt.setInt(3, (int) rec.y + (int) rec.height - 1);
							param_prep_stmt.setInt(4, (int) rec.x);
							param_prep_stmt.setInt(5, (int) rec.x + (int) rec.width - 1);

							ResultSet imgParamRslts = param_prep_stmt.executeQuery();
							int count = 0;
							while (imgParamRslts.next()) {
								int y = imgParamRslts.getInt("x");
								y = y - (int) rec.y;

								int x = imgParamRslts.getInt("y");
								x = x - (int) rec.x;

								for (int i = 1; i < 11; i++) {
									retVal[y][x][i - 1] = imgParamRslts.getFloat("p" + i);
								}
								count++;
							}

							if (count == (int) rec.height * (int) rec.width) {
								executed = true;
								gotParams = true;
							}
						} catch (SQLException ex) {
							ex.printStackTrace();
						}
					}

				} catch (SQLException ex) {
					ex.printStackTrace();
				} finally {
					if (con != null) {
						con.close();
					}
				}
			} catch (SQLException ex) {
				System.out.println(ex.getMessage());
			}
			tryCount++;
		}

		if (this.normalizer != null)
			this.normalizer.normalizeParameterValues(retVal);
		return retVal;
	}

	private String buildFileQueryString(CacheKey key) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = key.getEvent().getTimePeriod().getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		if (key.isLeftSide()) {
			queryString.append(" DESC;");
		} else {
			queryString.append(" ASC;");
		}
		return queryString.toString();
	}

	private String buildFileQueryString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}

		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT id, startdate FROM files_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE(((startdate BETWEEN ? AND ?) OR (enddate BETWEEN ? AND ?))");
		queryString.append(" AND (wavelength = ?)) ORDER BY startdate");
		queryString.append(" DESC;");
		return queryString.toString();
	}

	private String buildQueryParamString(CacheKey key) {
		// for constructing the table names from the year and month of
		// the event

		Interval period = key.getEvent().getTimePeriod();
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT x, y, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ");
		queryString.append("FROM image_params_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE ( file_id = ? ) AND (x BETWEEN ? AND ? ) AND (y BETWEEN ? AND ?);");
		return queryString.toString();
	}

	private String buildQueryParamString(Interval period) {
		// for constructing the table names from the year and month of
		// the event
		DateTime startTime = period.getStart();

		String calStartYear = "" + startTime.getYear();
		String calStartMonth;
		if ((startTime.getMonthOfYear()) < 10) {
			calStartMonth = "0" + (startTime.getMonthOfYear());
		} else {
			calStartMonth = "" + (startTime.getMonthOfYear());
		}
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT x, y, p1, p2, p3, p4, p5, p6, p7, p8, p9, p10 ");
		queryString.append("FROM image_params_");
		queryString.append(calStartYear);
		queryString.append(calStartMonth);
		queryString.append(" WHERE ( file_id = ? ) AND (x BETWEEN ? AND ? ) AND (y BETWEEN ? AND ?);");
		return queryString.toString();
	}

	@Override
	public DenseMatrix[] getImageParamForEv(IEvent event, ImageDBWaveParamPair[] params, boolean leftSide) {
		// Get the image parameters for each wavelength in the set of dimensions
		DenseMatrix[] dimMatArr = new DenseMatrix[params.length];
		for (int i = 0; i < params.length; i++) {

			double[][][] paramVals = this.getImageParamForWave(event, params[i].wavelength, leftSide);
			int rows = paramVals.length;
			int cols = paramVals[0].length;

			// get the param indicated by the dims array at depth i
			// the param value is from 1 to 10 whare array index is 0-9
			// hence the -1
			int paramIdx = params[i].parameter - 1;
			double[][] data = new double[rows][cols];
			for (int x = 0; x < cols; x++) {
				for (int y = 0; y < rows; y++) {
					data[y][x] = paramVals[y][x][paramIdx];
				}
			}
			dimMatArr[i] = new JMatrix(data);
		}
		return dimMatArr;
	}

	@Override
	public ImageDBDateIdPair[] getImageIdsForInterval(Interval period, Waveband wavelength) {
		ImageDBDateIdPair[] result = null;
		try {
			Connection con = null;
			try {
				con = this.dsourc.getConnection();
				con.setAutoCommit(true);

				DateTime startDateTime = period.getStart();
				DateTime endDateTime = period.getEnd();

				ArrayList<ImageDBDateIdPair> idList = new ArrayList<ImageDBDateIdPair>();

				boolean runAgain = false;
				do {
					Interval queryPeriod = new Interval(startDateTime, endDateTime);
					String queryFileString = buildFileQueryString(queryPeriod);
					Timestamp startTime = new Timestamp(queryPeriod.getStartMillis());
					Timestamp endTime = new Timestamp(queryPeriod.getEndMillis());

					PreparedStatement file_prep_stmt = con.prepareStatement(queryFileString);
					file_prep_stmt.setTimestamp(1, startTime);
					file_prep_stmt.setTimestamp(2, endTime);
					file_prep_stmt.setTimestamp(3, startTime);
					file_prep_stmt.setTimestamp(4, endTime);
					file_prep_stmt.setInt(5, Utility.convertWavebandToInt(wavelength));
					ResultSet imgFileIdResults = file_prep_stmt.executeQuery();

					while (imgFileIdResults.next()) {
						int id = imgFileIdResults.getInt("id");
						Timestamp ts = imgFileIdResults.getTimestamp(2);
						ImageDBDateIdPair pair = new ImageDBDateIdPair();
						pair.id = id;
						pair.period = new Interval(ts.getTime(), ts.getTime() + (1000 * 6 * 60));
						idList.add(pair);
					}
					imgFileIdResults.close();

					if (startDateTime.getMonthOfYear() == 12) {
						startDateTime = new DateTime(startDateTime.getYear() + 1, 1, 1, 0, 0, 0);
					} else {
						startDateTime = new DateTime(startDateTime.getYear(), startDateTime.getMonthOfYear() + 1, 1, 0,
								0, 0);
					}

					if (startDateTime.getYear() < endDateTime.getYear()) {
						runAgain = true;
					} else if (startDateTime.getYear() == endDateTime.getYear()) {
						if (startDateTime.getMonthOfYear() <= endDateTime.getMonthOfYear()) {
							runAgain = true;
						} else {
							runAgain = false;
						}
					} else {
						runAgain = false;
					}
				} while (runAgain);
				result = new ImageDBDateIdPair[idList.size()];
				for (int i = 0; i < result.length; i++)
					result[i] = idList.get(i);
			} catch (SQLException ex) {
				ex.printStackTrace();
			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return result;
	}

	@Override
	public DenseMatrix[] getImageParamForId(Interval period, int id) {
		String queryParamString = this.buildQueryParamString(period);
		DenseMatrix[] dimMatArr = new DenseMatrix[10];
		try {
			Connection con = this.dsourc.getConnection();
			con.setAutoCommit(true);
			try {
				PreparedStatement param_prep_stmt = con.prepareStatement(queryParamString);

				param_prep_stmt.setInt(1, id);
				param_prep_stmt.setInt(2, (int) 1);
				param_prep_stmt.setInt(3, (int) 64);
				param_prep_stmt.setInt(4, (int) 1);
				param_prep_stmt.setInt(5, (int) 64);

				ResultSet imgParamRslts = param_prep_stmt.executeQuery();

				double[][][] retVal = new double[64][64][10];
				while (imgParamRslts.next()) {
					int x = imgParamRslts.getInt("x");

					int y = imgParamRslts.getInt("y");

					for (int i = 1; i < 11; i++) {
						retVal[x - 1][y - 1][i - 1] = imgParamRslts.getFloat("p" + i);
					}
				}

				if (this.normalizer != null)
					this.normalizer.normalizeParameterValues(retVal);
				int rows = retVal[0].length;
				int cols = retVal.length;

				// get the param indicated by the dims array at depth i
				// the param value is from 1 to 10 whare array index is 0-9
				// hence the -1
				for (int paramIdx = 0; paramIdx < 10; paramIdx++) {
					double[][] data = new double[rows][cols];
					for (int x = 0; x < cols; x++) {
						for (int y = 0; y < rows; y++) {
							data[y][x] = retVal[y][x][paramIdx];
						}
					}
					dimMatArr[paramIdx] = new JMatrix(data);
				}

			} catch (SQLException ex) {
				ex.printStackTrace();

			} finally {
				if (con != null) {
					con.close();
				}
			}
		} catch (SQLException ex) {
			System.out.println(ex.getMessage());
		}
		return dimMatArr;
	}

	public BufferedImage getFullImgForId(Interval period, int id) throws SQLException, IOException {
		Connection con = null;
		BufferedImage bImg = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();
			// Calendar calStart = Calendar.getInstance();
			// calStart.setTimeInMillis(period.getStartMillis());

			String queryString2 = "SELECT image_file FROM image_files_" + startTime.getYear();
			if (startTime.getMonthOfYear() < 10) {
				queryString2 += "0" + (startTime.getMonthOfYear());
			} else {
				queryString2 += "" + (startTime.getMonthOfYear());
			}
			queryString2 += "_full WHERE file_id = ? ;";
			PreparedStatement img_prep_stmt = con.prepareStatement(queryString2);
			img_prep_stmt.setInt(1, id);

			ResultSet imgRslts = img_prep_stmt.executeQuery();

			if (imgRslts.next()) {
				Blob blob = imgRslts.getBlob(1);
				bImg = ImageIO.read(blob.getBinaryStream());
				blob.free();
			}
			imgRslts.close();
			img_prep_stmt.close();
		} finally {
			if (con != null) {
				con.close();
			}
		}
		return bImg;
	}

	@Override
	public ImageDBFitsHeaderData getHeaderForId(Interval period, int id) {
		Connection con = null;
		ImageDBFitsHeaderData headerData = null;
		try {
			con = this.dsourc.getConnection();
			con.setAutoCommit(true);

			// for pulling out the year and month for the table name to select
			// from
			DateTime startTime = period.getStart();
			// Calendar calStart = Calendar.getInstance();
			// calStart.setTimeInMillis(period.getStartMillis());

			String queryString = "SELECT x0, y0, rSun, dSun, cdelt FROM image_header_" + startTime.getYear();
			if ((startTime.getMonthOfYear()) < 10) {
				queryString += "0" + (startTime.getMonthOfYear());
			} else {
				queryString += "" + (startTime.getMonthOfYear());
			}
			queryString += " WHERE file_id = ? ;";
			PreparedStatement hdr_prep_stmt = con.prepareStatement(queryString);
			hdr_prep_stmt.setInt(1, id);

			ResultSet hdrRslts = hdr_prep_stmt.executeQuery();

			if (hdrRslts.next()) {
				headerData = new ImageDBFitsHeaderData();
				headerData.X0 = hdrRslts.getDouble(1);
				headerData.Y0 = hdrRslts.getDouble(2);
				headerData.R_SUN = hdrRslts.getDouble(3);
				headerData.DSUN = hdrRslts.getDouble(4);
				headerData.CDELT = hdrRslts.getDouble(5);
			}
			hdrRslts.close();
			hdr_prep_stmt.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return headerData;
	}
}