package edu.gsu.dmlab.ml;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.stream.IntStream;

import org.joda.time.DateTime;

import edu.gsu.dmlab.database.interfaces.IEventDBConnection;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.stats.interfaces.IFeatureValueProducer;
import smile.classification.NaiveBayes;
import smile.stat.distribution.Distribution;
import smile.stat.distribution.GaussianDistribution;

public class BayesTrainAndTest {

	IEventDBConnection objDBConn;
	IFeatureValueProducer prod;

	int numChoose = 4;
//	int numChoose = 1;
	int numIdx = 12;
//	int numIdx = 3;

	public BayesTrainAndTest(IEventDBConnection objDBConn, IFeatureValueProducer prod) {
		this.objDBConn = objDBConn;
		this.prod = prod;
	}

	public double[] run() {

		@SuppressWarnings("unchecked")
		List<IEvent>[] monthsArrAR = new List[12];
//		List<IEvent>[] monthsArrAR = new List[3];
		@SuppressWarnings("unchecked")
		List<IEvent>[] monthsArrCH = new List[12];
//		List<IEvent>[] monthsArrCH = new List[3];
		@SuppressWarnings("unchecked")
		List<IEvent>[] monthsArrQS = new List[12];
//		List<IEvent>[] monthsArrQS = new List[3];

		for (int i = 0; i < 12; i++) {
//		for (int i = 0; i < 3; i++) {
			monthsArrAR[i] = new ArrayList<IEvent>();
			monthsArrCH[i] = new ArrayList<IEvent>();
			monthsArrQS[i] = new ArrayList<IEvent>();
		}

		List<IEvent> arEvents = this.objDBConn.getAllEvents(EventType.ACTIVE_REGION);
		this.partitionMonths(arEvents, monthsArrAR);
		List<IEvent> chEvents = this.objDBConn.getAllEvents(EventType.CORONAL_HOLE);
		this.partitionMonths(chEvents, monthsArrCH);
		List<IEvent> qsEvents = this.objDBConn.getAllEvents(EventType.QUIET_SUN);
		this.partitionMonths(qsEvents, monthsArrQS);

		List<int[]> testSetIndexList = this.getTestIndexSets();
		System.out.println("Test Comb Count: " + testSetIndexList.size());

		double[] accResults = new double[testSetIndexList.size()];

		
		IntStream.range(0, testSetIndexList.size()).parallel().forEach(testIdx -> {
			int[] testSet = testSetIndexList.get(testIdx);

			// create a list of index values for training set
			LinkedList<Integer> trainIdxList = new LinkedList<Integer>();
			for (int i = 0; i < this.numIdx; i++) {
				boolean inTestSet = false;
				for (int val : testSet) {
					if (val == i)
						inTestSet = true;
				}

				if (!inTestSet)
					trainIdxList.add(Integer.valueOf(i));
			}

			@SuppressWarnings("unchecked")
			List<double[]>[] trainingArr = new List[3];
			List<double[]> arTrainingValues = new ArrayList<double[]>();
			trainingArr[0] = arTrainingValues;
			List<double[]> chTrainingValues = new ArrayList<double[]>();
			trainingArr[1] = chTrainingValues;
			List<double[]> qsTrainingValues = new ArrayList<double[]>();
			trainingArr[2] = qsTrainingValues;

			for (Integer idx : trainIdxList) {
				for (IEvent ev : monthsArrAR[idx]) {
					arTrainingValues.add(this.prod.getStats(ev));
				}

				for (IEvent ev : monthsArrCH[idx]) {
					chTrainingValues.add(this.prod.getStats(ev));
				}

				for (IEvent ev : monthsArrQS[idx]) {
					qsTrainingValues.add(this.prod.getStats(ev));
				}
			}

			NaiveBayes model = this.getModel(trainingArr);

			double testCount = 0;
			double correctCount = 0;
			for (int idx : testSet) {

				for (IEvent ev : monthsArrAR[idx]) {
					testCount++;
					int predVal = model.predict(this.prod.getStats(ev));
					if (predVal == 0) {
						correctCount++;
					}
				}

				for (IEvent ev : monthsArrCH[idx]) {
					testCount++;
					int predVal = model.predict(this.prod.getStats(ev));
					if (predVal == 1) {
						correctCount++;
					}
				}

				for (IEvent ev : monthsArrQS[idx]) {
					testCount++;
					int predVal = model.predict(this.prod.getStats(ev));
					if (predVal == 2) {
						correctCount++;
					}
				}
			}
			accResults[testIdx] = correctCount / testCount;

		});

		return accResults;
	}

	private NaiveBayes getModel(List<double[]>[] trainingArr) {
		double sampleCount = 0;
		for (int i = 0; i < trainingArr.length; i++)
			sampleCount += trainingArr[i].size();

		double[] classPriori = new double[3];
		for (int i = 0; i < trainingArr.length; i++)
			classPriori[i] = trainingArr[i].size() / sampleCount;

		Distribution[][] distributions = new Distribution[trainingArr.length][trainingArr[0].get(0).length];
		for (int i = 0; i < trainingArr.length; i++) {
			for (int j = 0; j < distributions[i].length; j++) {
				List<double[]> inputData = trainingArr[i];
				double[] data = new double[inputData.size()];
				int count = 0;
				for (double[] inputArr : inputData) {
					data[count++] = inputArr[j];
				}
				distributions[i][j] = new GaussianDistribution(data);
			}
		}

		NaiveBayes model = new NaiveBayes(classPriori, distributions);
		return model;
	}

	List<int[]> getTestIndexSets() {
		int[] input = new int[this.numIdx];
		for (int i = 0; i < input.length; i++)
			input[i] = i;

		List<int[]> subsets = new ArrayList<>();

		int[] s = new int[this.numChoose]; // keeping index values

		for (int i = 0; (s[i] = i) < this.numChoose - 1; i++)
			;
		subsets.add(this.getSubset(input, s));

		while (true) {
			int i;
			// find position
			for (i = this.numChoose - 1; i >= 0 && s[i] == this.numIdx - this.numChoose + i; i--)
				;
			if (i < 0) {
				break;
			}
			s[i]++; // increment this item
			for (++i; i < this.numChoose; i++) {// fill the remaining items
				s[i] = s[i - 1] + 1;
			}
			subsets.add(this.getSubset(input, s));
		}

		return subsets;
	}

	int[] getSubset(int[] input, int[] subset) {
		int[] result = new int[subset.length];
		for (int i = 0; i < subset.length; i++) {
			result[i] = input[subset[i]];
			// System.out.print(result[i]+",");
		}
		// System.out.println("");
		return result;
	}

	private void partitionMonths(List<IEvent> eventsList, List<IEvent>[] monthsArr) {
		for (IEvent ev : eventsList) {
			DateTime time = ev.getTimePeriod().getStart();
			int month = time.getMonthOfYear();
			monthsArr[month - 1].add(ev);
//			monthsArr[month % 3].add(ev);
//			monthsArr[month - 10].add(ev);
		}
	}

}
