package edu.gsu.dmlab.ml;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.stream.IntStream;

import org.joda.time.DateTime;

import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.stats.FMeasure;
import edu.gsu.dmlab.stats.interfaces.IFeatureValueProducer;
import smile.classification.RandomForest;

public class ForestTrainAndTest {

	List<double[]> allF1Scores = new LinkedList<>();
	
	IFeatureValueProducer prod;
	int numChoose = 4;
	int numIdx = 12;
	int nTrees = 60;

	private Random rand = new Random();

	public ForestTrainAndTest(IFeatureValueProducer prod) {
		this.prod = prod;
	}

	@Override
	public void finalize() throws Throwable {
		try {
			this.prod = null;
		} finally {
			super.finalize();
		}
	}

	
	public double[][] getAccuracyResults(List<IEvent>[] objectSets) {

		// Create array to partition each event list into the months that are in
		// it. We assume there are only 12 months of data.
		@SuppressWarnings("unchecked")
		final List<IEvent>[][] monthsArrList = new List[objectSets.length][this.numIdx];
		for (int j = 0; j < objectSets.length; j++) {
			for (int i = 0; i < this.numIdx; i++) {
				monthsArrList[j][i] = new ArrayList<IEvent>();
			}
		}

		// Partition each object set into the months that they occur in.
		for (int j = 0; j < objectSets.length; j++) {
			this.partitionMonths(objectSets[j], monthsArrList[j]);
		}

		final List<int[]> testSetIndexList = this.getTestIndexSets();

		final double[][] accResults = new double[testSetIndexList.size()][objectSets.length * objectSets.length];

		IntStream.range(0, testSetIndexList.size()).parallel().forEach(testIdx -> {
		
			int[] testSet = testSetIndexList.get(testIdx);

			// create a list of index values for training set
			LinkedList<Integer> trainIdxList = new LinkedList<Integer>();
			for (int i = 0; i < this.numIdx; i++) {
				boolean inTestSet = false;
				for (int val : testSet) {
					if (val == i) {
						inTestSet = true;
						break;
					}
				}

				if (!inTestSet)
					trainIdxList.add(Integer.valueOf(i));
			}

			// Create an array for holding the vectors for each event
			// from each of the classes in the training set.
			@SuppressWarnings("unchecked")
			List<double[]>[] trainingArr = new List[objectSets.length];
			for (int j = 0; j < trainingArr.length; j++) {
				trainingArr[j] = new ArrayList<double[]>();
			}

			// Add the vectors for the events in the training set to
			// their corresponding array location.
			for (Integer idx : trainIdxList) {
				// Loop over each event type.
				for (int j = 0; j < objectSets.length; j++) {
					// Get values for each event in the month marked as being in
					// the training set.
					for (IEvent ev : monthsArrList[j][idx]) {
						double[] val = this.prod.getStats(ev);//changed from .getFeatureValues(ev);
						trainingArr[j].add(val);
					}
				}
			}

			// Get rid of the list used to indicate what months were used for
			// the training set.
			trainIdxList.clear();
			trainIdxList = null;

			// Create an array for holding the vectors for each event
			// from each of the classes in the testing set.
			@SuppressWarnings("unchecked")
			List<double[]>[] testingArr = new List[objectSets.length];
			// Partition each object set into the months that they occur in.
			for (int j = 0; j < testingArr.length; j++) {
				testingArr[j] = new ArrayList<double[]>();
			}

			// Add the vectors for the events in the testing set to
			// their corresponding array location.
			for (Integer idx : testSet) {
				// Loop over each event type.
				for (int j = 0; j < objectSets.length; j++) {
					// Get values for each event in the month marked as being in
					// the testing set.
					for (IEvent ev : monthsArrList[j][idx]) {
						double[] val = this.prod.getStats(ev);//changed from .getFeatureValues(ev);
						testingArr[j].add(val);
					}
				}
			}

			// Now that we have the training and testing sets made, we need to
			// make sure they are balanced

			List<double[]>[] balancedTrainingArr = this.getUnderSampledSets(trainingArr);
			List<double[]>[] balancedTestingArr = this.getUnderSampledSets(testingArr);

			// Train the RandomForest model on the vectors in the
			// training set.
			RandomForest model = this.getModel(balancedTrainingArr);

			// Clear all the training sparse vectors now that the training
			// of the model has been completed.
			for (int j = 0; j < balancedTrainingArr.length; j++) {
				balancedTrainingArr[j].clear();
				balancedTrainingArr[j] = null;
			}
			balancedTrainingArr = null;

			// Test each of the objects in the testing dataset and count
			// which were classified correctly.
			double[][] classCount = new double[objectSets.length][objectSets.length];

			List<Integer> actualValues = new ArrayList<>();
			List<Integer> predictedValues = new ArrayList<>();

			// Test all of the events in the balanced testing set
			for (int j = 0; j < objectSets.length; j++) {
				// Process the values for this event type
				for (double[] val : balancedTestingArr[j]) {
					int predVal = model.predict(val);
					val = null;
					classCount[j][predVal]++;
					
					/* Prepare for f1-Score */
					actualValues.add(Integer.valueOf(j));
					predictedValues.add(Integer.valueOf(predVal));
				}
			}
			
			/** Calculate F1-Score for each class **/
			FMeasure fm = new FMeasure(convertListToArray(actualValues), convertListToArray(predictedValues), new int[]{0,1,2});
			allF1Scores.add(fm.measure());
			

			// Clear all the testing vectors now that the testing
			// of the model has been completed.
			for (int j = 0; j < objectSets.length; j++) {
				balancedTestingArr[j].clear();
				balancedTestingArr[j] = null;
			}
			balancedTestingArr = null;

			// Testing is done clear the model and the test set array
			testSet = null;
			model = null;

			// Add this iteration to the output list.
			for (int j = 0; j < objectSets.length; j++) {
				for (int k = 0; k < objectSets.length; k++) {
					accResults[testIdx][(j * objectSets.length) + k] = classCount[j][k];
				}
			}
			classCount = null;

			// Clear all the training and testing vectors now that we
			// have completed this round of testing.
			for (int j = 0; j < objectSets.length; j++) {
				trainingArr[j].clear();
				testingArr[j].clear();
				trainingArr[j] = null;
				testingArr[j] = null;
			}
			trainingArr = null;
			testingArr = null;
		});

		// clear out the list used to hold the combinations of testing set
		// indexes.
		testSetIndexList.clear();

		// Clean up the partitions of events into their months as we are done
		// with those partitions now.
		for (int j = 0; j < objectSets.length; j++) {
			for (int i = 0; i < this.numIdx; i++) {
				monthsArrList[j][i].clear();
				monthsArrList[j][i] = null;
			}
		}

		System.out.println("AccResult: " + Arrays.toString(smile.math.Math.colMeans(accResults)));
		return accResults;
	}

	private RandomForest getModel(List<double[]>[] trainingArr) {

		// calculate how many samples are in the training set.
		int sampleCount = 0;
		for (int i = 0; i < trainingArr.length; i++)
			sampleCount += trainingArr[i].size();

		double[][] x = new double[sampleCount][];
		int[] y = new int[sampleCount];

		int idxLoc = 0;
		for (int i = 0; i < trainingArr.length; i++) {
			// Get the training data for the class being processed.
			List<double[]> inputData = trainingArr[i];

			// Add the data to the input array, and put the class label in the corresponding
			// location on the label array.
			for (int j = 0; j < inputData.size(); j++) {

				// get the data
				double[] data = inputData.get(j);
				
				// place it into the sample array
				x[j + idxLoc] = data;

				// set the class label for this sample
				y[j + idxLoc] = i;
			}

			// update idxLoc for next class
			idxLoc += inputData.size();
		}

		// Create the model using the inputs and return
		RandomForest model = new RandomForest(x, y, this.nTrees);
		return model;
	}

	
	List<int[]> getTestIndexSets() {
		int[] input = new int[this.numIdx];
		for (int i = 0; i < input.length; i++)
			input[i] = i;

		List<int[]> subsets = new ArrayList<>();

		int[] s = new int[this.numChoose]; // keeping index values
		int[] temp = null;

		for (int i = 0; (s[i] = i) < this.numChoose - 1; i++)
			;
		subsets.add(this.getSubset(input, s));

		while (true) {
			int i;
			// find position
			for (i = this.numChoose - 1; i >= 0 && s[i] == this.numIdx - this.numChoose + i; i--)
				;
			if (i < 0) {
				break;
			}
			s[i]++; // increment this item
			for (++i; i < this.numChoose; i++) {// fill the remaining items
				s[i] = s[i - 1] + 1;
			}
			
			subsets.add(this.getSubset(input, s));//TODO: commented
		}

		return subsets;
	}

	int[] getSubset(int[] input, int[] subset) {
		int[] result = new int[subset.length];
		for (int i = 0; i < subset.length; i++) {
			result[i] = input[subset[i]];
		}
		return result;
	}

	private void partitionMonths(List<IEvent> eventsList, List<IEvent>[] monthsArr) {
		for (IEvent ev : eventsList) {
			DateTime time = ev.getTimePeriod().getStart();
			int month = time.getMonthOfYear();
			monthsArr[month - 1].add(ev);
			time = null;
		}
	}

	private List<double[]>[] getUnderSampledSets(List<double[]>[] arrOfObjs) {
		int min = arrOfObjs[0].size();
		for (int i = 1; i < arrOfObjs.length; i++) {
			if (min > arrOfObjs[i].size())
				min = arrOfObjs[i].size();
		}

		@SuppressWarnings("unchecked")
		List<double[]>[] balancedArr = new List[arrOfObjs.length];
		for (int i = 0; i < arrOfObjs.length; i++) {
			balancedArr[i] = this.getUnderSampledSet(arrOfObjs[i], min);
		}
		return balancedArr;
	}

	private List<double[]> getUnderSampledSet(List<double[]> vals, int size) {
		List<double[]> tmpEvents = new LinkedList<double[]>();
		for (double[] val : vals)
			tmpEvents.add(val);

		List<double[]> results = new ArrayList<double[]>();
		for (int i = 0; i < size; i++) {
			results.add(tmpEvents.remove(this.rand.nextInt(tmpEvents.size())));
		}
		tmpEvents.clear();
		tmpEvents = null;

		return results;
	}


	private int[] convertListToArray(List<Integer> l) {
		
		int[] res = new int[l.size()];
		for(int i = 0; i < res.length; i++) {
			res[i] = l.get(i);
		}
		return res;
	}
	
	public List<double[]> getAllF1Scores() {
		return allF1Scores;
	}
}
