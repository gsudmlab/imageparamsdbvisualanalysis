package edu.gsu.dmlab;

import java.sql.SQLException;

import org.joda.time.DateTime;

import edu.gsu.dmlab.classification.ClassificationPerParam;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.statanalysis.ParamAnalysis;
import edu.gsu.dmlab.statanalysis.TimeDiffAnalysis;

public class ParamsVisualization {

	public static void main(String[] args) {

		/*-------------------------------------------
		 * Uncomment to compute all time differences.
		 *-------------------------------------------*/
		computeAllTimeDifferences();
		
		/*-------------------------------------------
		 * Uncomment to compute mean of each image parameter for one year
		 *-------------------------------------------*/
//		computeMeanOfAllParams();		
		
		/*-------------------------------------------
		 * Uncomment to compute Random Forest on new or old image parameters.
		 *-------------------------------------------*/
//		runClassification();

	}
	
	/**
	 * See the javadoc of the class 'TimeDiffAnalysis'.
	 */
	private static void computeAllTimeDifferences() {
		
		DateTime startTime = new DateTime(2012, 1, 1, 0, 0, 0, 0);
		Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
				Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };
//		Waveband[] wavelengths = { Waveband.AIA171 };

		TimeDiffAnalysis tds = null;

		for (Waveband w : wavelengths) {
			tds = new TimeDiffAnalysis("image_params_2012", startTime, w, "TimeDiff_Minutes_2012_01");
			try {
				tds.getTimeDifferences2();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * See the javadoc of the class 'ParamAnalysis'.
	 */
	private static void computeMeanOfAllParams() {
		
		DateTime startTime = new DateTime(2012, 1, 1, 0, 0, 0, 0);
		Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
				Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };
		ParamAnalysis tds = null;
		for (Waveband w : wavelengths) {
			tds = new ParamAnalysis("image_params_2012", startTime, w, "CSVs");
			tds.run();
		}
	}
	
	/**
	 * See the javadoc of the class 'ClassificationPerParam'.
	 */
	private static void runClassification() {
		ClassificationPerParam cls = new ClassificationPerParam();
		cls.runRandomForest();
	}
}
