package edu.gsu.dmlab.config;

import java.io.File;

import javax.sql.DataSource;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.commons.dbcp2.BasicDataSource;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;

public class ConfigReader {

	private DataSource imgDBPoolSourc = null;
	private DataSource imgDBPoolSourc2 = null;
	private DataSource imgDBProduction = null;
	private DataSource objDBPoolSourc = null;
	private int imageCacheSize;

	public ConfigReader(String fileLoc) throws InvalidConfigException {
		this.config(fileLoc);
	}

	public DataSource getImageDBPoolSource1() {
		return this.imgDBPoolSourc;
	}

	public DataSource getImageDBPoolSource2() {
		return this.imgDBPoolSourc2;
	}
	
	public DataSource getImageDBProduction() {
		return this.imgDBProduction;
	}

	public DataSource getObjDBPoolSource() {
		return this.objDBPoolSourc;
	}

	public int getCacheSize() {
		return this.imageCacheSize;
	}

	// /////////////////////////////////////////////////////////////////////////////////
	// Start of private methods
	// ////////////////////////////////////////////////////////////////////////////////
	private void config(String folderLocation) throws InvalidConfigException {
		try {
			// TODO: still need to implement reading lambda values.
			DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
			Document doc;
			String fileLoc = folderLocation + File.separator + "paramsvisualization.cfg.xml";
			DocumentBuilder bldr = fctry.newDocumentBuilder();
			doc = bldr.parse(new File(fileLoc));
			doc.getDocumentElement().normalize();

			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();
			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "imagepool":
						this.imgDBPoolSourc = this.getPoolSourc(nde.getChildNodes());
						break;
					case "imagepool2":
						this.imgDBPoolSourc2 = this.getPoolSourc(nde.getChildNodes());
						break;
					case "imageproduction":
						this.imgDBProduction = this.getPoolSourc(nde.getChildNodes());
						break;
					case "objectpool":
						this.objDBPoolSourc = this.getPoolSourc(nde.getChildNodes());
						break;
					case "imagedbcache":
						this.imageCacheSize = Integer.parseInt(this.getAttrib(nde, "max"));
						break;
					}

				}
			}

		} catch (Exception e) {
			throw new InvalidConfigException("Config failed with: " + e.getMessage());
		}

	}

	private DataSource getPoolSourc(NodeList ndLst) {
		BasicDataSource dbPoolSourc = null;
		dbPoolSourc = new BasicDataSource();
		dbPoolSourc.setPoolPreparedStatements(false);
		dbPoolSourc.setDefaultAutoCommit(true);

		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {

				case "timeout":
					String idlStr = this.getAttrib(nde, "value");
					dbPoolSourc.setDefaultQueryTimeout(Integer.parseInt(idlStr));
					dbPoolSourc.setSoftMinEvictableIdleTimeMillis(Integer.parseInt(idlStr));
					break;
				case "minpool":
					String minStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMinIdle(Integer.parseInt(minStr));
					break;
				case "maxpool":
					String maxStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxIdle(Integer.parseInt(maxStr));
					break;
				case "maxsize":
					String maxszStr = this.getAttrib(nde, "value");
					dbPoolSourc.setMaxTotal(Integer.parseInt(maxszStr));
					break;
				case "username":
					dbPoolSourc.setUsername(this.getAttrib(nde, "value"));
					break;
				case "password":
					dbPoolSourc.setPassword(this.getAttrib(nde, "value"));
					break;
				case "validationquery":
					dbPoolSourc.setValidationQuery(this.getAttrib(nde, "value"));
					break;
				case "driverclass":
					dbPoolSourc.setDriverClassName(this.getAttrib(nde, "value"));
					break;
				case "url":
					dbPoolSourc.setUrl(this.getAttrib(nde, "value"));
					break;
				default:
					System.out.print("Unknown Element: ");
					System.out.println(ndName);
				}
			}
		}
		return dbPoolSourc;
	}

	private String getAttrib(Node prntNde, String attName) {
		StringBuffer buf = new StringBuffer("");
		boolean isSet = false;
		if (prntNde.hasAttributes()) {
			NamedNodeMap ndeMp = prntNde.getAttributes();
			for (int i = 0; i < ndeMp.getLength(); i++) {
				Node nde = ndeMp.item(i);
				if (nde.getNodeName().compareTo(attName) == 0) {
					buf.append(nde.getNodeValue());
					isSet = true;
					break;
				}
			}
		}

		if (!isSet) {
			return "";
		} else {
			return buf.toString();
		}
	}

}
