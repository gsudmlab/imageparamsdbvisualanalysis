package edu.gsu.dmlab.statanalysis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;

import au.com.bytecode.opencsv.CSVWriter;
import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.database.ImageParamDBConnection2;
import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.databases.ImageDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;

/**
 * This class is written to collect the time differences [in minutes] between image
 * parameter files for a period of 1 year (2012), starting from a given start time,
 * for a given AIA channel.
 * The results will be stored in (12 * 9) csv files, each of which represents the time
 * differences corresponding to one month of the year, and one wavelength.
 *   
 * @author azim
 *
 */
public class TimeDiffAnalysis {

	private String tableExtension;
	private DateTime startOfMonth;
	private Waveband wavelength;
	private String outputDir;

	public TimeDiffAnalysis(String tableExt, DateTime startTime, Waveband wave, String outputDir) {
		this.tableExtension = tableExt;
		this.startOfMonth = startTime;
		this.wavelength = wave;
		this.outputDir = outputDir;
	}

	/**
	 * @deprecated
	 * Reads each of the 12 tables (of the given year), one at a time, and then
	 * retrieves ID, sTime, eTime, timeDiff, and stores the data of each month
	 * in a separate csv file.
	 * Problem: In the schema "images_production_db", time stamps are generated
	 * regardless of the presence or absence of image data. Therefore, as long
	 * as this issue exists, this method won't show the real gaps in the tables.
	 * See 'getTimeDifferences2'.
	 */
	public void getTimeDifferences() {
		String[] tableExtensions = this.makeAllExtensions(this.tableExtension);
		IImageParamDBConnection dbconn = null;
		DateTime endTime = null;
		Interval per = null;
		int minDiff = 0;

		File directory = new File(this.outputDir);
		if (!directory.exists()) {
			directory.mkdir();
		}

		List<List<String>> allRows = null;
		for (int i = 0; i < tableExtensions.length; i++) {
			dbconn = createConnection(tableExtensions[i]);
			endTime = startOfMonth.plusMonths(1);

			System.out.println("\tStart: " + this.startOfMonth.toString());
			System.out.println("\t\tEnd: " + endTime.toString());

			per = new Interval(this.startOfMonth, endTime);
			ImageDBDateIdPair[] pairs = dbconn.getImageIdsForInterval(per, this.wavelength);

			allRows = new LinkedList<>();
			List<String> row = null;
			ImageDBDateIdPair p = null;
			/*-
			 * While reading the results:
			 * 	1. Parse the array in reverse, since the time
			 * 	stamps are retrieved in reverse.
			 * 	2. Ignore the last element since that is
			 * 	always the first date in the next month.
			 */
			for (int j = pairs.length - 2; j > 0; j--) {
				p = pairs[j];
				row = new LinkedList<>();

				minDiff = Minutes.minutesBetween(p.period.getStart(), p.period.getEnd()).getMinutes();
				row.add(p.id + "");
				row.add(p.period.getStart().toString());
				row.add(p.period.getEnd().toString());
				row.add(minDiff + "");
				allRows.add(row);
			}
			
			String outFileName = directory.getAbsolutePath() + File.separator + "TimeDiff_minutes_" + this.wavelength
					+ "_" + tableExtensions[i] + ".csv";
			
			System.out.println("In total: [" + pairs.length + "] ids are retrieved.");
			System.out.println("The results are stored to: [" + outFileName + "]");
			this.write(allRows, outFileName);
			this.startOfMonth = endTime;
		}

		System.out.println("Done!");

	}

	/**
	 * Reads each of the 12 tables (of the given year), one at a time, and computes
	 * the time differences between consecutive instances. As the result, the
	 * followings columns will be stored in a csv file:
	 * 		"ID", "START", "NEXTSTART", "DIFF_mins"
	 * The entire outcome is for one particular wavelengths.
	 * 
	 * @throws SQLException
	 */
	public void getTimeDifferences2() throws SQLException {
		
		IImageDBCreator dbcreator = this.createDBCreator();
		
		String[] tableExtensions = this.makeAllExtensions(this.tableExtension);
		IImageParamDBConnection dbconn = null;
		DateTime endOfMonth = null;
		Interval oneMonthPeriod = null;
		int diffInMinutes = 0;

		File directory = new File(this.outputDir);
		if (!directory.exists()) {
			directory.mkdir();
		}

		List<List<String>> allRows = null;
		for (int i = 0; i < tableExtensions.length; i++) {
			
			if(!tableExtensions[i].endsWith("01"))
				continue;
			dbconn = createConnection(tableExtensions[i]);
			endOfMonth = startOfMonth.plusMonths(1);
			
			oneMonthPeriod = new Interval(this.startOfMonth, endOfMonth);
			// Get all <id, period> for one month
			ImageDBDateIdPair[] pairs = dbconn.getImageIdsForInterval(oneMonthPeriod, this.wavelength);
			
			System.out.println("\tWave: ["+ this.wavelength + "]");
			System.out.println("\tPeriod: [" + this.startOfMonth.toString() + "]-[" + endOfMonth.toString() + "]");
			System.out.println("\tTOTAL NO. of IDs: [" + pairs.length + "]");
			
			allRows = new LinkedList<>();
			List<String> row = null;
			ImageDBDateIdPair p = null, pp = null;
			DateTime s1Time = null; //start-time corresponding to an existing image
			DateTime s2Time = null; //start-time corresponding to the next existing image
			/*-
			 * While reading the results:
			 * 	1. Parse the array in reverse, since the time stamps are retrieved
			 *  in reverse.
			 * 	2. Ignore the last element since that is always the first date in
			 *  the next month. In addition, j iterates to find the beginning of
			 *  the time differences, while k is for the end of the differences.
			 *  Hence, 'j > 1' and k > 0.
			 */
			for (int j = pairs.length - 2; j > 1; j--) {
				p = pairs[j];
				s1Time = p.period.getStart();
				// If s1Time is valid (i.e., 4096 rows exists for that), look for the next (s2Time)
				if(dbcreator.checkParamsExist(p.id, p.period)) {
					row = new LinkedList<>();
					while(j > 0) {
						j--;
						pp = pairs[j];
						if(dbcreator.checkParamsExist(pp.id, pp.period)) {
							// If s2Time is valid (i.e., 4096 rows exists for that), break the while loop.
							s2Time = pp.period.getStart();
							j++;
							break;
						}
					}
				
					// Compute time difference only if a valid sTime and eTime is found.
					diffInMinutes = Minutes.minutesBetween(s1Time, s2Time).getMinutes();
					row.add(p.id + "");
					row.add(s1Time.toString());
					row.add(s2Time.toString());
					row.add(diffInMinutes + "");
					allRows.add(row);
					
//					System.out.println("s1--[" + p.id + "]--[" + s1Time + "]---TO---" +
//							"s2--[" + pp.id + "]--[" + s2Time + "]------[" + diffInMinutes + "] mins");
				}
				
				if(j % 100 == 0) {
					System.out.println("\t\tProcessed: [" + (pairs.length - j) + "/" + pairs.length + "]");
				}
				
			}
			
			String outFileName = directory.getAbsolutePath() + File.separator + "TimeDiff_minutes_" + this.wavelength
					+ "_" + tableExtensions[i] + ".csv";
			
			System.out.println("In total: [" + pairs.length + "] ids are retrieved.");
			System.out.println("The results are stored to: [" + outFileName + "]");
			this.write(allRows, outFileName);
			this.startOfMonth = endOfMonth;
		}
		System.out.println("Done!");
	}
	/**
	 * 
	 * @return
	 */
	private IImageDBCreator createDBCreator() {
		ConfigReader conf;
		DataSource dstor;
		IImageDBCreator dbcreator = null;

		try {
			conf = new ConfigReader("config");
			dstor = conf.getImageDBProduction();
			System.out.println("Connection: " + dstor.getConnection().toString());
			dbcreator = new ImageDBCreator(dstor);
		} catch (InvalidConfigException | SQLException e) {
			e.printStackTrace();
		}
		return dbcreator;
	}
	
	/**
	 * Establish the connection to the database.
	 * 
	 * @return
	 */
	private IImageParamDBConnection createConnection(String tableExtension) {

		IImageParamDBConnection dbconn = null;

		ConfigReader conf;
		try {
			conf = new ConfigReader("config");
			DataSource dstor = conf.getImageDBProduction();
//			System.out.println("Connection: " + dstor.getConnection().toString());
			dbconn = new ImageParamDBConnection2(dstor, null, 64, 10, 64, conf.getCacheSize(), tableExtension);
		} catch (InvalidConfigException e) {
			e.printStackTrace();
		}
		return dbconn;
	}

	/**
	 * Make table extension for 12 months, matching the table names in the database.
	 * 
	 * @param tableExt
	 * @return
	 */
	private String[] makeAllExtensions(String tableExt) {

		String[] allExtensions = new String[12];

		String ending = "";
		for (int i = 1; i <= 12; i++) {
			ending = i >= 10 ? "" + i : "0" + i;
			allExtensions[i - 1] = tableExt + ending;
		}
		return allExtensions;
	}

	/**
	 * 
	 * @param allRows
	 * @param fileName
	 */
	private void write(List<List<String>> allRows, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = { "ID", "START", "NEXTSTART", "DIFF_mins" };
			csvOutput.writeNext(row);

			for (List<String> r : allRows) {
				row[0] = r.get(0);
				row[1] = r.get(1);
				row[2] = r.get(2);
				row[3] = r.get(3);
				csvOutput.writeNext(row);
			}
			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
