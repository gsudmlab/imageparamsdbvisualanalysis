package edu.gsu.dmlab.statanalysis;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.joda.time.Minutes;

import au.com.bytecode.opencsv.CSVWriter;
import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.database.ImageParamDBConnection2;
import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.databases.ImageDBCreator;
import edu.gsu.cs.dmlab.databases.interfaces.IImageDBCreator;
import edu.gsu.cs.dmlab.datatypes.ImageDBDateIdPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;
import smile.math.matrix.DenseMatrix;

/**
 * This class is written to collect the time difference [in minutes] between
 * image parameter files for a period of 1 year, starting from a given stat
 * time, for a given AIA channel. The results will be stored in 12 csv files,
 * each of which represents the time differences corresponding to one month of
 * the year.
 * 
 * @author azim
 *
 */
public class ParamAnalysis {

	private String tableExtension;
	private DateTime startTime;
	private Waveband wavelength;
	private String outputDir;

	public ParamAnalysis(String tableExt, DateTime startTime, Waveband wave, String outputDir) {
		this.tableExtension = tableExt;
		this.startTime = startTime;
		this.wavelength = wave;
		this.outputDir = outputDir;
	}

	/**
	 * Reads each of the 12 tables (of the given year), one at a time, and then
	 * retrieves the image parameters per ID and computes the 3-number summary
	 * (i.e., min, max, mean).
	 */
	public void run() {	
		String[] tableExtensions = this.makeAllExtensions(this.tableExtension);
		IImageParamDBConnection dbconn = null;
		DateTime endTime = null;
		Interval per = null;

		File directory = new File(this.outputDir);
		if (!directory.exists()) {
			directory.mkdir();
		}

		List<List<String>> allRows = null;
		// Iterate over 12 months (DB tables)
		for (int i = 0; i < tableExtensions.length; i++) {
			
			dbconn = createConnection(tableExtensions[i]);
			endTime = startTime.plusMonths(1);

			System.out.println("\tStart: " + this.startTime.toString());
			System.out.println("\tEnd: " + endTime.toString());

			per = new Interval(this.startTime, endTime);

			ImageDBDateIdPair[] pairs = dbconn.getImageIdsForInterval(per, this.wavelength);
			
			allRows = new LinkedList<>();
			List<String> row = null;
			ImageDBDateIdPair p = null;
			/*-
			 * While reading the resultant IDs:
			 * 	1. Parse the the array in reverse, since the time
			 * 	stamps are retrieved in reverse.
			 * 	2. Ignore the last element since it is
			 * 	always the first date in the proceeding month.
			 */
			for (int j = pairs.length - 2; j > 0; j--) {
				p = pairs[j];
				row = new LinkedList<>();
				row.add(p.id + "");
				DenseMatrix[] matrices = dbconn.getImageParamForId(p.period, p.id);
				// System.out.println("--> For ID: " + p.id);
				/*
				 * Compute the mean value for each of the ten image params.
				 */
				for (int k = 0; k < matrices.length; k++) {

					double m = smile.math.Math.mean(smile.math.Math.rowMeans((matrices[k].array())));
					m = ((int) (m * 10000)) / 10000.000;
					row.add(m + "");
				}
				allRows.add(row);
				if (j % 10 == 0)
					System.out.println("\r\t[" + j + "/" + pairs.length + "]");
			}

			String outFileName = directory.getAbsolutePath() + File.separator + "ImgParamsMean" + this.wavelength + "_"
					+ tableExtensions[i] + ".csv";

			System.out.println("In total: [" + pairs.length + "] ids are retrieved.");
			System.out.println("The results are stored to: [" + outFileName + "]");
			this.write(allRows, outFileName);
			this.startTime = endTime;
		}
		System.out.println("Done!");

	}

	/**
	 * Establish the connection to the database.
	 * 
	 * @return
	 */
	private IImageParamDBConnection createConnection(String tableExtension) {

		IImageParamDBConnection dbconn = null;

		ConfigReader conf;
		try {
			conf = new ConfigReader("config");
			DataSource dstor = conf.getImageDBProduction();
			System.out.println("Connection: " + dstor.getConnection().toString());
			dbconn = new ImageParamDBConnection2(dstor, null, 64, 10, 64, conf.getCacheSize(), tableExtension);
		} catch (InvalidConfigException | SQLException e) {
			e.printStackTrace();
		}
		return dbconn;
	}

	/**
	 * Make table extension for 12 months, matching the table names in the database.
	 * 
	 * @param tableExt
	 * @return
	 */
	private String[] makeAllExtensions(String tableExt) {

		String[] allExtensions = new String[12];

		String ending = "";
		for (int i = 1; i <= 12; i++) {
			ending = i >= 10 ? "" + i : "0" + i;
			allExtensions[i - 1] = tableExt + ending;
		}
		return allExtensions;
	}

	/**
	 * 
	 * @param allRows
	 * @param fileName
	 */
	private void write(List<List<String>> allRows, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = { "ID", "ENTROPY", "MEAN", "STD-DEVIATION", "FRACTAL-DIM", "SKEWNESS", "KURTOSIS",
					"UNIFORMITY", "SMOOTHNESS", "CONTRAST", "DIRECTIONALITY" };
			csvOutput.writeNext(row);

			for (List<String> r : allRows) {
				int i = 0;
				for (String s : r) {
					row[i++] = s;
				}
				csvOutput.writeNext(row);
			}
			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
