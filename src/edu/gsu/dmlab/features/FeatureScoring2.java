package edu.gsu.dmlab.features;

import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.stream.Collectors;

import org.apache.commons.math3.exception.DimensionMismatchException;
import org.apache.commons.math3.exception.NullArgumentException;
import org.apache.commons.math3.stat.inference.OneWayAnova;

import com.google.common.math.Quantiles;
import com.google.common.math.Quantiles.Scale;

import au.com.bytecode.opencsv.CSVWriter;
import edu.gsu.dmlab.database.interfaces.IEventDBConnection;
import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.dmlab.datatypes.FeatureScore;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.cs.dmlab.util.Utility;
import smile.math.Math;
import smile.math.matrix.DenseMatrix;

/**
 * This is a copy of FeatureScoring class, that is modified
 * to produce the data for the distribution of min of events
 * for each even type. Similarly, for q1, q2, q3, max, sd, mean.
 * 
 * @author azim
 *
 */
public class FeatureScoring2 {

	IEventDBConnection evdb;
	IImageParamDBConnection dbconn;
	Random rand = new Random();
	OneWayAnova ow = new OneWayAnova();

	public FeatureScoring2(IEventDBConnection evdb, IImageParamDBConnection dbconn) {
		this.evdb = evdb;
		this.dbconn = dbconn;

	}

	/**
	 * This function calculates 7-number summary for one single wave-setting
	 * parameter, every time that it is called. It retrieves all the ar/ch/qs
	 * events that took place within a specified period of time (e.g., one
	 * month) and their corresponding regions, so that it can pull those regions
	 * from the database of parameters, and calculate the 7-number summary on
	 * each of the regions of parameters. It repeats the process of calculating
	 * each of the 7 statistics on the regions for all the events (for a
	 * particular wave and setting) ten times, and then it uses ANOVA test to
	 * measure how separate the three distributions (e.g., min(ar), min(ch),
	 * min(qs) are and assign a score to that particular settings.
	 * 
	 * @param params
	 *            One or more image parameters from the list of the ten
	 *            parameters whose values are in the data base.
	 * @return a list of FeatureScore objects from which the following
	 *         information can be retrieved: wave index, setting index, and
	 *         FScore assigned to each statistics of that particular
	 *         wave-setting.
	 */
	public List<FeatureScore> getFeatureFStatScoring(ImageDBWaveParamPair[] params) {

		List<IEvent> arEvents = this.evdb.getAllEvents(EventType.ACTIVE_REGION);
		int minLen = arEvents.size();

		List<IEvent> chEvents = this.evdb.getAllEvents(EventType.CORONAL_HOLE);
		if (chEvents.size() < minLen)
			minLen = chEvents.size();

		List<IEvent> qsEvents = this.evdb.getAllEvents(EventType.QUIET_SUN);
		if (qsEvents.size() < minLen)
			minLen = qsEvents.size();

		List<FeatureScore> results = new ArrayList<FeatureScore>();
		for (int j = 0; j < params.length; j++) {
			for (int i = 0; i < 7; i++) {
				FeatureScore f = new FeatureScore();
				f.statId = i;
				f.param = params[j];
				results.add(f);
			}
		}

		List<IEvent> balencedAREvents = this.getUnderSampledSet(arEvents, minLen);
		List<IEvent> balancedCHEvents = this.getUnderSampledSet(chEvents, minLen);
		List<IEvent> balancedQSEvents = this.getUnderSampledSet(qsEvents, minLen);
		
		// A list of 7 arrays: arr1: all mins, arr2: all Q1, arr3: all Q2, ...
		List<double[]> arFeatureSet = this.getFeatureValues(balencedAREvents, params);
		List<double[]> chFeatureSet = this.getFeatureValues(balancedCHEvents, params);
		List<double[]> qsFeatureSet = this.getFeatureValues(balancedQSEvents, params);
		
		// The rest is removed. Not needed for this short experiment.

		
		String fName = "ar_w_" + params[0].wavelength.toString() + "_p_" + params[0].parameter + ".csv";
		write(arFeatureSet, fName);
		
		fName = "ch_w_" + params[0].wavelength.toString() + "_p_" + params[0].parameter + ".csv";
		write(chFeatureSet, fName);
		
		fName = "qs_w_" + params[0].wavelength.toString() + "_p_" + params[0].parameter + ".csv";
		write(qsFeatureSet, fName);
		
		return results;
	}

	private List<IEvent> getUnderSampledSet(List<IEvent> events, int size) {
		List<IEvent> tmpEvents = new LinkedList<IEvent>();
		for (IEvent ev : events)
			tmpEvents.add(ev);

		List<IEvent> results = new ArrayList<IEvent>();
		for (int i = 0; i < size; i++) {
			results.add(tmpEvents.remove(this.rand.nextInt(tmpEvents.size())));
		}

		return results;
	}

	private List<double[]> getFeatureValues(List<IEvent> events, ImageDBWaveParamPair[] params) {
		
		ArrayList<double[]> results = new ArrayList<double[]>();
		List<List<double[]>> intermediateResults = events.parallelStream().map(ev -> {
			return this.calculateStats(ev, params);
		}).collect(Collectors.toList());

		for (int i = 0; i < params.length; i++) {
			for (int j = 0; j < 7; j++) {
				int count = 0;
				double[] featValues = new double[intermediateResults.size()];
				for (List<double[]> evFeatlist : intermediateResults) {
					featValues[count++] = evFeatlist.get(i)[j];
				}
				results.add(featValues);
			}
		}

		return results;
	}

	private List<double[]> calculateStats(IEvent ev, ImageDBWaveParamPair[] params) {
		Scale s = Quantiles.quartiles();
		//Get a list of regions for different parameters. (For now params.length = 1,
		//so paramVals.length = 1 as well.)
		DenseMatrix[] paramVals = this.dbconn.getImageParamForEv(ev, params, true);

		ArrayList<double[]> results = new ArrayList<double[]>();
		//convert the dense matrix to a 1D and a 2D array
		for (DenseMatrix pV : paramVals) {

			ArrayList<Double> d = new ArrayList<Double>();
			double[][] arr = pV.array();
			double[] arr2 = new double[arr.length * arr[0].length];
			int count = 0;
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[0].length; j++) {
					d.add(Double.valueOf(arr[i][j]));
					arr2[count++] = arr[i][j];
				}
			}
			/*
			 * Calculate the 7-number summary for each parameter (d) of an image, of a
			 * particular wavelength
			 */
			Map<Integer, Double> m = s.indexes(1, 2, 3).compute(d);// Compute the 1st, 2nd, and 3rd quantiles
			double maxVal = Math.max(arr);
			double minVal = Math.min(arr);
			double avg = Math.mean(arr2);
			double std = Math.sqr(Math.var(arr2));

			double[] ans = new double[7];
			ans[0] = minVal;
			ans[1] = m.get(1).doubleValue();
			ans[2] = m.get(2).doubleValue();
			ans[3] = m.get(3).doubleValue();
			ans[4] = maxVal;
			ans[5] = avg;
			ans[6] = std;
			results.add(ans);
		}
		return results;
	}
	
	private void write(List<double[]> allStats, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = { "MIN", "Q1", "Q2", "Q3", "MAX", "MEAN", "SD" };
			csvOutput.writeNext(row);
			//allScores: [ [FS.min, FS.q1, FS.q2, ..., FS.Std], ...., [FS.min, FS.q1, FS.q2, ..., FS.Std] ]
			//where each of those stats is stored as a FeatureScore object
			int n = allStats.get(0).length;
			for(int i = 0; i < n; i++) {
				row[0] = Double.toString(allStats.get(0)[i]);//i-th element in min
				row[1] = Double.toString(allStats.get(1)[i]);//i-th element in q1
				row[2] = Double.toString(allStats.get(2)[i]);//i-th element in q2
				row[3] = Double.toString(allStats.get(3)[i]);//i-th element in q3
				row[4] = Double.toString(allStats.get(4)[i]);//i-th element in max
				row[5] = Double.toString(allStats.get(5)[i]);//i-th element in mean
				row[6] = Double.toString(allStats.get(6)[i]);//i-th element in sd
				csvOutput.writeNext(row);
			}

			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
}
