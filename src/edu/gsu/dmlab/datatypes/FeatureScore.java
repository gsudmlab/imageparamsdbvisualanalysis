package edu.gsu.dmlab.datatypes;

import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;

public class FeatureScore {
	public ImageDBWaveParamPair param;
	public int statId;
	public double fstatScore;
}
