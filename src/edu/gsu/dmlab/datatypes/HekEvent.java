package edu.gsu.dmlab.datatypes;

import java.awt.Polygon;
import java.awt.Rectangle;
import java.util.ArrayList;

import org.joda.time.Interval;

import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.geometry.Point2D;

public class HekEvent {
	int eventID;
	Interval period;
	EventType type;
	private Polygon poly = null;
	private Rectangle bBox = null;
	private Point2D location = null;
	int bboxArea;

	public HekEvent(int eventID, EventType type, Interval period, String pixBBox, String pixCenterLocation,
			String pixShape, int bboxArea) {
		this.eventID = eventID;
		this.period = period;
		this.type = type;
		this.poly = this.getPoly(pixShape);
		this.bBox = poly.getBounds();
		this.location = this.getPoint(pixCenterLocation);
		this.bboxArea = bboxArea;
	}

	public int getEventID() {
		return eventID;
	}

	public Interval getPeriod() {
		return period;
	}

	public EventType getType() {
		return this.type;
	}

	public Point2D getLocation() {
		return this.location;
	}

	public Rectangle getBBox() {
		return this.bBox;
	}

	public Polygon getShape() {
		return this.poly;
	}

	public int getBBoxArea() {
		return this.bboxArea;
	}

	/**
	 * getPoly: returns a objectList of 2D points extracted from the input
	 * string the objectList of points are assumed to create a polygon, they are
	 * not tested
	 * 
	 * @param pointsString
	 *            :the string to extract the points from
	 * @return :returns the objectList of 2D points
	 */
	private Polygon getPoly(String pointsString) {
		pointsString = this.removeBrackets(pointsString);
		String[] pointsStrings = pointsString.split(",");
		String xy;
		ArrayList<Integer> xPoints = new ArrayList<Integer>();
		ArrayList<Integer> yPoints = new ArrayList<Integer>();
		for (int i = 0; i < pointsStrings.length; i++) {
			xy = pointsStrings[i];
			Point2D pnt = this.getPoint(xy);
			xPoints.add((int) pnt.x);
			yPoints.add((int) pnt.y);
		}
		int[] xArr = new int[xPoints.size()];
		int[] yArr = new int[yPoints.size()];
		for (int i = 0; i < xArr.length; i++) {
			xArr[i] = xPoints.get(i);
			yArr[i] = yPoints.get(i);
		}

		Polygon poly = new Polygon(xArr, yArr, xArr.length);
		return poly;
	}

	/**
	 * getPoint :returns a 2D point from the input string
	 * 
	 * @param xy
	 *            :input string containing x and y coordinate
	 * @return :the 2D point extracted from the string
	 */
	private Point2D getPoint(String xy) {
		int spaceIdx = xy.indexOf(' ');
		double x = Double.parseDouble(xy.substring(0, spaceIdx));
		double y = Double.parseDouble(xy.substring(spaceIdx));
		return new Point2D(x, y);
	}

	/**
	 * removeBrackets : used to remove anything preceeding or following a ( or a
	 * )
	 * 
	 * @param in
	 *            : the string to trim up
	 * @return : the trimmed string
	 */
	private String removeBrackets(String in) {
		int begin = in.indexOf('(');
		int end = in.lastIndexOf(')');
		while (begin >= 0 || end >= 0) {
			in = in.substring(begin + 1, end);
			begin = in.indexOf('(');
			end = in.lastIndexOf(')');
		}
		return in;
	}

}
