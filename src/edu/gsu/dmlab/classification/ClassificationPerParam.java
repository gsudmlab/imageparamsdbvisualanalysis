package edu.gsu.dmlab.classification;

import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import javax.sql.DataSource;

import org.joda.time.DateTime;

import au.com.bytecode.opencsv.CSVWriter;
import edu.gsu.dmlab.config.ConfigReader;
import edu.gsu.dmlab.database.EventDBConnection;
import edu.gsu.dmlab.database.ImageParamDBConnection2;
import edu.gsu.dmlab.database.interfaces.IEventDBConnection;
import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.datatypes.EventType;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.cs.dmlab.exceptions.InvalidConfigException;
import edu.gsu.cs.dmlab.imageproc.ImgParamNormalizer;
import edu.gsu.cs.dmlab.imageproc.interfaces.IImgParamNormalizer;
import edu.gsu.dmlab.ml.BayesTrainAndTest;
import edu.gsu.dmlab.ml.ForestTrainAndTest;
import edu.gsu.dmlab.stats.SatatsFeatureProducer;
import edu.gsu.dmlab.stats.interfaces.IFeatureValueProducer;

/**-
 * Before using this class, make sure that:
 * 	1. The correct schema is being read:
 * 		conf.getImageDBPoolSource1(); //for Juan's params
 * 		conf.getImageDBProduction();  //for tuned params
 * 	2. The min and max values are appropriate for the chosen schema.
 * 	3. The period is set to one year. This is used in:
 * 		this.evdb.getAllEvents(EventType.ACTIVE_REGION);
 * 	4. The x and y values as the coordinates of each pixel, is set
 * 	appropriately. At this time, x and y is populated reversely in the
 *	schema 'images_production_db'. See the method 'fetchImageParams'
 *	in the class 'ImageParamDBConnection2'.
 *	5. This conducts a univariate classification. 
 * @author Azim Ahmadzadeh
 *
 */
public class ClassificationPerParam {

	IImageParamDBConnection dbconn2;
	IEventDBConnection evdb;

	public ClassificationPerParam() {

		ConfigReader conf;
		try {
			conf = new ConfigReader("config");
			DataSource dstor = conf.getObjDBPoolSource();
			this.evdb = new EventDBConnection(dstor);

			String tableExtension = "";

			DataSource dsourc = conf.getImageDBPoolSource1(); //Juan's params
			//DataSource dsourc = conf.getImageDBPoolSource2();
			//DataSource dsourc = conf.getImageDBProduction(); //New params
			int paramDownSample = 64;
			int paramDim = 10;
			int paramCells = 64;
			
			/*
			 * Create a map between the min-max and each parameter to be used for 0-1-normalization.
			 * Both min and max values are empirically found (from the values in the DB) to be
			 * larger than the 99-th percentile or less than the 1-st percentile of the distributions,
			 * respectively. These values are used to prevent the normalizer to squeeze the data range
			 * too harshly.
			 */
			/* NEW PARAMS
			HashMap<Integer, double[]> paramRangeMap = new HashMap<Integer, double[]>();
			paramRangeMap.put(Integer.valueOf(1), new double[] { 0.0, 4.1   }); // Entropy
			paramRangeMap.put(Integer.valueOf(2), new double[] { 0.0, 190.0 }); // Mean
			paramRangeMap.put(Integer.valueOf(3), new double[] { 0.0, 37.2  });	// StD
			paramRangeMap.put(Integer.valueOf(4), new double[] { 0.0, 1.925 });	// F-Dim
			paramRangeMap.put(Integer.valueOf(5), new double[] { 0.0, 24.0 	});	// Skewness
			paramRangeMap.put(Integer.valueOf(6), new double[] { 0.0, 800.0 });	// Kurtosis
			paramRangeMap.put(Integer.valueOf(7), new double[] { 0.0305, 1.0});	// Uniformity
			paramRangeMap.put(Integer.valueOf(8), new double[] { 0.0, 1.0 	});	// Rel-Smoothness
			paramRangeMap.put(Integer.valueOf(9), new double[] { 0.0, 1350.0});	// T-Contrast
			paramRangeMap.put(Integer.valueOf(10), new double[] { 0.0, 1.7 	});	// T-Dir
			*/
			
			/* JUAN's PARAMS */
			HashMap<Integer, double[]> paramRangeMap = new HashMap<Integer, double[]>();
			paramRangeMap.put(Integer.valueOf(1), new double[] { 0.0, 6.5   }); // Entropy
			paramRangeMap.put(Integer.valueOf(2), new double[] { 0.0, 200.0 }); // Mean
			paramRangeMap.put(Integer.valueOf(3), new double[] { 0.0, 43.2  });	// StD
			paramRangeMap.put(Integer.valueOf(4), new double[] { 0.0, 1.705 });	// F-Dim
			paramRangeMap.put(Integer.valueOf(5), new double[] { 0.0, 15.9 	});	// Skewness
			paramRangeMap.put(Integer.valueOf(6), new double[] { 0.0, 254.0 });	// Kurtosis
			paramRangeMap.put(Integer.valueOf(7), new double[] { 0.0, 1.0});	// Uniformity
			paramRangeMap.put(Integer.valueOf(8), new double[] { 0.0, 0.0039});	// Rel-Smoothness
			paramRangeMap.put(Integer.valueOf(9), new double[] { 0.0, 36.0437});// T-Contrast
			paramRangeMap.put(Integer.valueOf(10), new double[] { 0.0, 0.0250});// T-Dir
			
			IImgParamNormalizer normalizer = new ImgParamNormalizer(paramRangeMap);
			this.dbconn2 = new ImageParamDBConnection2(dsourc, normalizer, paramDownSample, paramDim, paramCells,
					conf.getCacheSize(), tableExtension);

		} catch (InvalidConfigException e) {
			e.printStackTrace();
		}

	}

	/**
	 * Random Forest
	 */
	public void runRandomForest() {
		Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
				Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };

		System.out.println("Getting all the events ...");
		/* Random Forest Classifier */
		List<IEvent>[] objectSets = new List[3];
		objectSets[0] = this.evdb.getAllEvents(EventType.ACTIVE_REGION);
		objectSets[1] = this.evdb.getAllEvents(EventType.CORONAL_HOLE);
		objectSets[2] = this.evdb.getAllEvents(EventType.QUIET_SUN);
		
		ImageDBWaveParamPair[] params = null;
		
		// Loop for all ten image parameters
		for(int p = 1; p <= 10; p++) { 
			
			System.out.println("--PARAM [" + p + "]:");
			
			params = new ImageDBWaveParamPair[wavelengths.length];
			// Loop for all 9 wavelengths
			for (int i = 0; i < wavelengths.length; i++) {
				ImageDBWaveParamPair pair = new ImageDBWaveParamPair();
				pair.parameter = p; //1:Entropy, 2:MEAN, ...
				pair.wavelength = wavelengths[i];
				params[i] = pair;
			}

			System.out.println("\tProducing features ...");
			IFeatureValueProducer prod = new SatatsFeatureProducer(this.dbconn2, params);
			
			System.out.println("\tRunning Random Forest classifier ...");
			ForestTrainAndTest tester = new ForestTrainAndTest(prod);

			System.out.println("\tGetting the accuracies ...");
			// Compute the confusion matrix for (12 choose 4) trials.
			double[][] vals = tester.getAccuracyResults(objectSets);
			// Get the f1-score for each class per trials.
			List<double[]> fScores = tester.getAllF1Scores();

			DateTime dateTime = new DateTime();
			String cTime = dateTime.toString().substring(0, 19).replaceAll("[-,:,T]", "_");
			
			System.out.println("\tSaving the results ...");
			String fileName = "P[" + p + "]_OLD_RForest_confusionMatrix_" + cTime + ".csv";
			this.write(vals, fileName);

			fileName = "P[" + p + "]_OLD_RForest_F1Scores_" + cTime + ".csv";
			this.write(fScores, fileName);
			
			System.out.println("--RANDOM FOREST for PARAM [" + p + "] IS DONE!");
			System.out.println("---------------------------------------------\n\n");
		}
		
	}
	
	public void runBayes() {
		Waveband[] wavelengths = { Waveband.AIA94, Waveband.AIA131, Waveband.AIA171, Waveband.AIA193, Waveband.AIA211,
				Waveband.AIA304, Waveband.AIA335, Waveband.AIA1600, Waveband.AIA1700 };

		ImageDBWaveParamPair[] params = new ImageDBWaveParamPair[wavelengths.length];
		for (int i = 0; i < wavelengths.length; i++) {
			ImageDBWaveParamPair pair = new ImageDBWaveParamPair();
			pair.parameter = 1;
			pair.wavelength = wavelengths[i];
			params[i] = pair;
		}

		/* Naive Bayes Classifier */
		IFeatureValueProducer prod = new SatatsFeatureProducer(this.dbconn2, params);
		BayesTrainAndTest tester = new BayesTrainAndTest(this.evdb, prod);
		double[] vals = tester.run();

		DateTime dateTime = new DateTime();
		String cTime = dateTime.toString().substring(0, 19).replaceAll("[-,:,T]", "_");

		String fileName = "P2_new_Bayes_" + cTime + ".csv";
		this.write(vals, fileName);
	}
	
	/**
	 * 
	 * @param allScores
	 * @param fileName
	 */
	private void write(double[] allScores, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = { "Classification Accuracy" };
			csvOutput.writeNext(row);
			for (double val : allScores) {
				row = new String[1];
				row[0] = Double.toString(val);
				csvOutput.writeNext(row);
			}
			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	/**
	 * @param allScores
	 * @param fileName
	 */
	private void write(double[][] allScores, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = null;
			for (double[] val : allScores) {
				row = new String[val.length];
				for (int i = 0; i < val.length; i++) {
					row[i] = Double.toString(val[i]);
				}

				csvOutput.writeNext(row);
			}
			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}

	/**
	 * 
	 * @param allScores
	 * @param fileName
	 */
	private void write(List<double[]> allScores, String fileName) {
		try {
			CSVWriter csvOutput = new CSVWriter(new FileWriter(fileName, false), ',');
			String[] row = null;
			for (double[] val : allScores) {
				row = new String[val.length];
				for (int i = 0; i < val.length; i++) {
					row[i] = Double.toString(val[i]);
				}

				csvOutput.writeNext(row);
			}
			csvOutput.close();

		} catch (IOException e) {

			e.printStackTrace();
		}
	}


}
