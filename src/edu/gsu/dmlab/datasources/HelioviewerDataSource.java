package edu.gsu.dmlab.datasources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.imageio.ImageIO;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.w3c.dom.Document;

import edu.gsu.dmlab.datasources.interfaces.IImageProcDataSource;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.dmlab.exceptions.ExternalDatasourceException;

/**
 * @author Dustin Kempton, Data Mining Lab, Georgia State University
 */
public class HelioviewerDataSource implements IImageProcDataSource {

	SSLContext ctx;
	HashMap<Waveband, Integer> helioviewerWave_IdMap;
	private final String USER_AGENT = "GSU DMLab Java Client 0.0.2";

	DateTimeFormatter queryDTFormatter = DateTimeFormat.forPattern("yyyy-MM-dd'T'HH:mm:ss'Z'");

	String jp2URL = "https://api.helioviewer.org/v2/getJP2Image/";
	String closestIdURL = "https://api.helioviewer.org/v2/getClosestImage/";
	String headerURL = "https://api.helioviewer.org/v2/getJP2Header/";

	String charset = java.nio.charset.StandardCharsets.UTF_8.name();
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();

	int maxTry = 5;

	private static class DefaultTrustManager implements X509TrustManager {

		@Override
		public void checkClientTrusted(X509Certificate[] arg0, String arg1) throws CertificateException {
			// TODO Auto-generated method stub
		}

		@Override
		public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
			// TODO Auto-generated method stub
		}

		@Override
		public X509Certificate[] getAcceptedIssuers() {
			// TODO Auto-generated method stub
			return null;
		}
	}

	public HelioviewerDataSource() {
		this.helioviewerWave_IdMap = new HashMap<Waveband, Integer>();
		this.helioviewerWave_IdMap.put(Waveband.AIA94, Integer.valueOf(8));
		this.helioviewerWave_IdMap.put(Waveband.AIA131, Integer.valueOf(9));
		this.helioviewerWave_IdMap.put(Waveband.AIA171, Integer.valueOf(10));
		this.helioviewerWave_IdMap.put(Waveband.AIA193, Integer.valueOf(11));
		this.helioviewerWave_IdMap.put(Waveband.AIA211, Integer.valueOf(12));
		this.helioviewerWave_IdMap.put(Waveband.AIA304, Integer.valueOf(13));
		this.helioviewerWave_IdMap.put(Waveband.AIA335, Integer.valueOf(14));
		this.helioviewerWave_IdMap.put(Waveband.AIA1600, Integer.valueOf(15));
		this.helioviewerWave_IdMap.put(Waveband.AIA1700, Integer.valueOf(16));

		CookieHandler.setDefault(new CookieManager(null, CookiePolicy.ACCEPT_ALL));
		try {
			TrustManager[] trustManagers = { new DefaultTrustManager() };
			this.ctx = SSLContext.getInstance("TLS");
			this.ctx.init(null, trustManagers, null);
		} catch (NoSuchAlgorithmException | KeyManagementException e) {
			e.printStackTrace();
		}
	}

	@Override
	public BufferedImage getImage(DateTime date, Waveband wavelength) {
		int tryCount = 0;
		while (tryCount < 5) {
			try {
				String query = String.format("date=%s&sourceId=%s", this.queryDTFormatter.print(date),
						URLEncoder.encode(helioviewerWave_IdMap.get(wavelength).toString(), this.charset));

				URL url = new URL(this.jp2URL + "?" + query);
				HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();

				connection.setConnectTimeout(30000);
				connection.setReadTimeout(60000);
				connection.setSSLSocketFactory(this.ctx.getSocketFactory());

				connection.setRequestProperty("Accept-Charset", this.charset);
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent", USER_AGENT);
				connection.setRequestProperty("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
				connection.setUseCaches(false);
				connection.setDoOutput(true);

				connection.connect();

				if (connection.getResponseCode() == 200) {

					byte[] responseBytes = null;
					try (InputStream response = connection.getInputStream()) {
						try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
							byte[] buffer = new byte[1024];
							int length = 0;

							while ((length = response.read(buffer)) != -1) {
								baos.write(buffer, 0, length);
							}
							responseBytes = baos.toByteArray();
						}
					} finally {
						connection.disconnect();
					}

					// Jpeg200 files consist of chunks with 8 byte headers.
					// So, if it is not at least 8 bytes, it is invalid.
					if (responseBytes != null && responseBytes.length > 7) {
						if (checkJP2(responseBytes)) {
							try (InputStream imgStr = new ByteArrayInputStream(responseBytes)) {
								BufferedImage img = ImageIO.read(imgStr);
								return img;
							}
						} else {
							throw new ExternalDatasourceException("Response failed JP2 Check.");
						}
					}
					throw new ExternalDatasourceException("Header of JP2 response to small.");

				} else {
					String respStr = "";
					try (InputStream response = connection.getErrorStream()) {
						try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
							byte[] buffer = new byte[1024];
							int length = 0;
							while ((length = response.read(buffer)) != -1) {
								baos.write(buffer, 0, length);
							}
							respStr = baos.toString(this.charset);
						}

					} finally {
						connection.disconnect();
					}
					throw new ExternalDatasourceException(respStr);
				}
			} catch (SocketTimeoutException e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} catch (Exception e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(3000);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} finally {
				tryCount++;
			}
		}
		throw new ExternalDatasourceException("Unable to get Image.");
	}

	static final int JP2_SIGNATURE_BOX = 0x6a502020;
	static final int FILE_TYPE_BOX = 0x66747970;
	static final int FT_BR = 0x6a703220;

	private boolean checkJP2(byte[] byteBuffer) {
		// We make sure the first 12 bytes are the JP2 signature box
		int idx = 0;

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != 0x0000000c)
				return false;
			idx += 4;
		} else {
			return false;
		}

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != JP2_SIGNATURE_BOX)
				return false;
			idx += 4;
		} else {
			return false;
		}

		if ((idx + 4) < byteBuffer.length) {
			if (this.readInt(byteBuffer, idx) != 0x0d0a870a)
				return false;
			idx += 4;
		} else {
			return false;
		}

		return this.readFileTypeBox(byteBuffer, idx);
	}

	/**
	 * Reads a signed int (i.e., 32 bit) from the input. Prior to reading, the input
	 * should be realigned at the byte level.
	 * 
	 * @return The next byte-aligned signed int (32 bit) from the input.
	 */
	public final int readInt(byte[] byteBuffer, int pos) {
		return (((byteBuffer[pos++] & 0xFF) << 24) | ((byteBuffer[pos++] & 0xFF) << 16)
				| ((byteBuffer[pos++] & 0xFF) << 8) | (byteBuffer[pos++] & 0xFF));
	}

	final long readLong(byte[] byteBuffer, int pos) {
		return (((long) (byteBuffer[pos++] & 0xFF) << 56) | ((long) (byteBuffer[pos++] & 0xFF) << 48)
				| ((long) (byteBuffer[pos++] & 0xFF) << 40) | ((long) (byteBuffer[pos++] & 0xFF) << 32)
				| ((long) (byteBuffer[pos++] & 0xFF) << 24) | ((long) (byteBuffer[pos++] & 0xFF) << 16)
				| ((long) (byteBuffer[pos++] & 0xFF) << 8) | ((long) (byteBuffer[pos++] & 0xFF)));
	}

	/**
	 * This method reads the File Type box.
	 *
	 * @return false if the File Type box was not found or invalid else true
	 *
	 * @exception java.io.IOException
	 *                If an I/O error occurred.
	 * @exception java.io.EOFException
	 *                If the end of file was reached
	 */
	public boolean readFileTypeBox(byte[] byteBuffer, int pos) {
		int length;

		int nComp;
		boolean foundComp = false;

		// Read box length (LBox)
		length = this.readInt(byteBuffer, pos);
		if (length == 0) { // This can not be last box
			System.out.println("Zero-length of Profile Box");
			return false;
		}
		pos += 4;

		// Check that this is a File Type box (TBox)
		if (this.readInt(byteBuffer, pos) != FILE_TYPE_BOX) {
			System.out.println("Bad File Type Box");
			return false;
		}
		pos += 4;

		// Check for XLBox
		if (length == 1) { // Box has 8 byte length;
			System.out.println("File Too Big");
			return false;
		}

		// Read Brand field
		// in.readInt();
		pos += 4;

		// Read MinV field
		// in.readInt();
		pos += 4;

		// Check that there is at least one FT_BR entry in in
		// compatibility list
		nComp = (length - 16) / 4; // Number of compatibilities.
		for (int i = nComp; i > 0; i--) {
			if (this.readInt(byteBuffer, pos) == FT_BR)
				foundComp = true;
			pos += 4;
		}
		if (!foundComp) {
			System.out.println("No FT_BR entry.");

			return false;
		}

		return true;
	}

	@Override
	public ImageDBFitsHeaderData getHeader(DateTime date, Waveband wavelength) {
		int id = this.getClosestImageDescriptr(date, wavelength);
		if (id < 0)
			return null;

		int tryCount = 0;
		while (tryCount < this.maxTry) {
			try {
				String query = String.format("id=%s", id);
				URL url = new URL(this.headerURL + "?" + query);
				// System.out.println(url.toString());
				HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(30000);
				connection.setSSLSocketFactory(this.ctx.getSocketFactory());

				connection.setRequestProperty("Accept-Charset", this.charset);
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent", USER_AGENT);
				connection.setRequestProperty("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
				connection.setUseCaches(false);
				connection.setDoOutput(true);

				connection.connect();

				if (connection.getResponseCode() == 200) {
					try (InputStream response = connection.getInputStream()) {
						try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
							byte[] buffer = new byte[1024];
							int length = 0;
							while ((length = response.read(buffer)) != -1) {
								baos.write(buffer, 0, length);
							}

							if (baos.size() > 24) {
								try (InputStream res = new ByteArrayInputStream(baos.toByteArray())) {
									DocumentBuilder builder = this.dbFactory.newDocumentBuilder();

									Document doc = builder.parse(res);
									return this.readFitsHeader(doc);
								} catch (Exception e) {
									e.printStackTrace();
									System.out.println(baos.toString(this.charset));
								}
							}
						}
					} finally {
						connection.disconnect();
					}
				}
			} catch (SocketTimeoutException e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} catch (Exception e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(300);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} finally {
				tryCount++;
			}
		}
		return null;
	}

	private ImageDBFitsHeaderData readFitsHeader(Document headerDocument) {

		ImageDBFitsHeaderData headerData = new ImageDBFitsHeaderData();

		String x0String = headerDocument.getElementsByTagName("X0_MP").item(0).getTextContent();
		String y0String = headerDocument.getElementsByTagName("Y0_MP").item(0).getTextContent();
		String rSunString = headerDocument.getElementsByTagName("R_SUN").item(0).getTextContent();
		String dSunString = headerDocument.getElementsByTagName("DSUN_OBS").item(0).getTextContent();
		String cDeltString = headerDocument.getElementsByTagName("CDELT1").item(0).getTextContent();
		headerData.X0 = Double.parseDouble(x0String);
		headerData.Y0 = Double.parseDouble(y0String);
		headerData.R_SUN = Double.parseDouble(rSunString);
		headerData.DSUN = Double.parseDouble(dSunString);
		headerData.CDELT = Double.parseDouble(cDeltString);
		return headerData;

	}

	private int getClosestImageDescriptr(DateTime date, Waveband wavelength) throws ExternalDatasourceException {

		int tryCount = 0;
		while (tryCount < this.maxTry) {
			try {
				String query = String.format("date=%s&sourceId=%s", this.queryDTFormatter.print(date),
						URLEncoder.encode(helioviewerWave_IdMap.get(wavelength).toString(), this.charset));

				URL url = new URL(this.closestIdURL + "?" + query);
				// System.out.println(url.toString());

				HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
				connection.setConnectTimeout(5000);
				connection.setReadTimeout(15000);
				connection.setSSLSocketFactory(this.ctx.getSocketFactory());

				connection.setRequestProperty("Accept-Charset", this.charset);
				connection.setRequestMethod("GET");
				connection.setRequestProperty("User-Agent", USER_AGENT);
				connection.setRequestProperty("Accept",
						"text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8");
				connection.setRequestProperty("Accept-Language", "en-US,en;q=0.8");
				connection.setUseCaches(false);
				connection.setDoOutput(true);

				connection.connect();

				if (connection.getResponseCode() == 200) {

					String responseString = null;
					try (InputStream response = connection.getInputStream()) {
						try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
							byte[] buffer = new byte[1024];
							int length = 0;
							while ((length = response.read(buffer)) != -1) {
								baos.write(buffer, 0, length);
							}
							responseString = baos.toString(this.charset);
						}
					} finally {
						connection.disconnect();
					}
					if (responseString != null) {
						responseString = responseString.substring(1, responseString.length() - 1);
						String[] respArr = responseString.split(",");

						if (!respArr[0].contains("error")) {
							String idString = respArr[0].substring(respArr[0].indexOf(':') + 1).replace('"', ' ')
									.trim();

							int id = Integer.parseInt(idString);

							return id;
						}
					}
				}
			} catch (SocketTimeoutException e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(30000);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} catch (Exception e) {
				if (tryCount >= this.maxTry - 1)
					throw new ExternalDatasourceException(e.getMessage(), e.getCause());
				try {
					Thread.sleep(300);
				} catch (InterruptedException e1) {
					// Do nothing.
				}
			} finally {
				tryCount++;
			}
		}
		return -1;
	}

}
