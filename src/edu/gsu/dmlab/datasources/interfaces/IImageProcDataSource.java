package edu.gsu.dmlab.datasources.interfaces;

import java.awt.image.BufferedImage;

import org.joda.time.DateTime;

import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.Waveband;

public interface IImageProcDataSource {
	
	public BufferedImage getImage(DateTime date, Waveband wavelength);
	
	public ImageDBFitsHeaderData getHeader(DateTime date, Waveband wavelength);
	
}
