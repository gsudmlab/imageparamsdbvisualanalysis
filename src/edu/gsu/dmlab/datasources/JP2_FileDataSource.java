package edu.gsu.dmlab.datasources;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NavigableSet;
import java.util.TreeSet;

import javax.imageio.ImageIO;
import javax.imageio.ImageReader;
import javax.imageio.metadata.IIOMetadata;
import javax.imageio.stream.ImageInputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.joda.time.DateTime;
import org.joda.time.Minutes;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.google.common.base.Charsets;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.hash.HashFunction;
import com.google.common.hash.Hashing;

import edu.gsu.dmlab.datasources.interfaces.IImageProcDataSource;
import edu.gsu.cs.dmlab.datatypes.ImageDBFitsHeaderData;
import edu.gsu.cs.dmlab.datatypes.Waveband;
import edu.gsu.cs.dmlab.util.Utility;


public class JP2_FileDataSource implements IImageProcDataSource {

	LoadingCache<CacheKey, NavigableSet<TimeFilePair>> cache = null;
	HashFunction hashFunct = Hashing.murmur3_128();
	int cadenceMin;
	String base;

	private class CacheKey {
		String key;
		int hashInt;

		public CacheKey(String directory, HashFunction hashFunct) {
			this.key = directory;
			this.hashInt = hashFunct.newHasher().putString(this.key, Charsets.UTF_8).hash().asInt();
		}

		public String getKey() {
			return this.key;
		}

		@Override
		public int hashCode() {
			return this.hashInt;
		}

		@Override
		public boolean equals(Object obj) {
			if (obj instanceof CacheKey) {
				CacheKey val = (CacheKey) obj;
				return val.getKey().equals(this.key);
			} else {
				return false;
			}
		}

		@Override
		public String toString() {
			return this.key;
		}
	}

	private class TimeFilePair {
		public DateTime timeOfFile = null;
		public Path path = null;
	}

	public JP2_FileDataSource(String baseDir, int maxCacheSize, int cadenceMin) {
		this.base = baseDir;
		this.cadenceMin = cadenceMin;
		this.cache = CacheBuilder.newBuilder().maximumSize(maxCacheSize)
				.build(new CacheLoader<CacheKey, NavigableSet<TimeFilePair>>() {
					public NavigableSet<TimeFilePair> load(CacheKey key) {
						System.out.println("Get Set");
						return getDirContents(key);
					}
				});
	}

	private NavigableSet<TimeFilePair> getDirContents(CacheKey key) {
		Comparator<TimeFilePair> comp = new Comparator<TimeFilePair>() {
			public int compare(TimeFilePair p1, TimeFilePair p2) {
				return p1.timeOfFile.compareTo(p2.timeOfFile);
			}
		};
		NavigableSet<TimeFilePair> result = new TreeSet<TimeFilePair>(comp);
		Path dir = Paths.get(key.getKey());
		try (DirectoryStream<Path> stream = Files.newDirectoryStream(dir, "*.jp2")) {
			for (Path p : stream) {
				String[] dParts = p.toString().split("/");
				String[] fParts = dParts[dParts.length - 1].split("_");
				dParts = null;

				DateTime timeOfFile = new DateTime(Integer.parseInt(fParts[0]), Integer.parseInt(fParts[1]),
						Integer.parseInt(fParts[2]), Integer.parseInt(fParts[4]), Integer.parseInt(fParts[5]));

				TimeFilePair pair = new TimeFilePair();
				pair.path = p;
				pair.timeOfFile = timeOfFile;
				result.add(pair);
			}
		} catch (IOException e1) {
			e1.printStackTrace();
		}

		return result;
	}

	private File getClosestFile(DateTime timeOfFile, Waveband wavelength) {

		StringBuilder sb = new StringBuilder();
		sb.append(this.base);
		sb.append(File.separator);
		sb.append(timeOfFile.getYear());
		sb.append(File.separator);
		sb.append(timeOfFile.getYear());
		String month;
		if ((timeOfFile.getMonthOfYear()) < 10) {
			month = "0" + (timeOfFile.getMonthOfYear());
		} else {
			month = "" + (timeOfFile.getMonthOfYear());
		}
		sb.append(month);
		sb.append(File.separator);
		String day;
		if ((timeOfFile.getDayOfMonth()) < 10) {
			day = "0" + (timeOfFile.getDayOfMonth());
		} else {
			day = "" + (timeOfFile.getDayOfMonth());
		}
		sb.append(day);
		sb.append(File.separator);
		sb.append(Utility.convertWavebandToInt(wavelength));

		CacheKey key = new CacheKey(sb.toString(), this.hashFunct);
		NavigableSet<TimeFilePair> set = this.cache.getUnchecked(key);

		TimeFilePair p = new TimeFilePair();
		p.timeOfFile = timeOfFile;
		p = set.floor(p);

		if (p != null) {
			int diff = 0;
			if (timeOfFile.isBefore(p.timeOfFile)) {
				diff = Minutes.minutesBetween(timeOfFile, p.timeOfFile).getMinutes();
			} else {
				diff = Minutes.minutesBetween(p.timeOfFile, timeOfFile).getMinutes();
			}
			System.out.println(timeOfFile.toString());
			System.out.println("Diff: " + diff);
			if (Math.abs(diff) <= this.cadenceMin) {
				File file = p.path.toFile();
				return file;
			}
		}

		return null;

	}

	@Override
	public BufferedImage getImage(DateTime date, Waveband wavelength) {

		BufferedImage results = null;
		try {
			File file = this.getClosestFile(date, wavelength);
			if (file != null) {
				try (ImageInputStream iis = ImageIO.createImageInputStream(file)) {
					Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);

					if (readers.hasNext()) {
						// pick the first available ImageReader
						ImageReader reader = readers.next();

						// attach source to the reader
						reader.setInput(iis, true);
						results = reader.read(0);
					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}

	@Override
	public ImageDBFitsHeaderData getHeader(DateTime date, Waveband wavelength) {

		ImageDBFitsHeaderData results = null;
		try {
			File file = this.getClosestFile(date, wavelength);
			if (file != null) {
				try (ImageInputStream iis = ImageIO.createImageInputStream(file)) {
					Iterator<ImageReader> readers = ImageIO.getImageReaders(iis);

					if (readers.hasNext()) {
						// pick the first available ImageReader
						ImageReader reader = readers.next();

						// attach source to the reader
						reader.setInput(iis, true);

						// read metadata of first image
						IIOMetadata metadata = reader.getImageMetadata(0);
						results = this.getHeaderData(metadata);

					}
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return results;
	}

	ImageDBFitsHeaderData getHeaderData(IIOMetadata metadata) {
		String[] names = metadata.getMetadataFormatNames();
		int length = names.length;

		Document doc = null;
		for (int i = 0; i < length; i++) {
			if (names[i].contains("javax_imageio"))
				doc = this.getFitsDoc(metadata.getAsTree(names[i]));
		}

		ImageDBFitsHeaderData header = null;
		if (doc != null) {
			Element root = doc.getDocumentElement();
			NodeList ndLst = root.getChildNodes();

			// Get the fits data node from the list children of the root of the xml document
			// Then get the children of that node
			NodeList fitsList = null;
			for (int i = 0; i < ndLst.getLength(); i++) {
				Node nde = ndLst.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "fits":
						fitsList = nde.getChildNodes();
						break;
					}
				}
			}

			// If we found the children of the fits node, then process
			// them to populate the header data.
			if (fitsList != null) {
				header = new ImageDBFitsHeaderData();
				for (int i = 0; i < fitsList.getLength(); i++) {
					Node nde = fitsList.item(i);
					if (nde.getNodeType() == Node.ELEMENT_NODE) {
						String ndName = nde.getNodeName();

						switch (ndName) {
						case "X0_MP":
							header.X0 = Double.parseDouble(nde.getTextContent());
							break;
						case "Y0_MP":
							header.Y0 = Double.parseDouble(nde.getTextContent());
							break;
						case "DSUN_OBS":
							header.DSUN = Double.parseDouble(nde.getTextContent());
							break;
						case "R_SUN":
							header.R_SUN = Double.parseDouble(nde.getTextContent());
							break;
						case "CDELT1":
							header.CDELT = Double.parseDouble(nde.getTextContent());
							break;
						}
					}
				}
			}
		}

		return header;
	}

	Document getFitsDoc(Node metadata) {
		Document doc = null;

		// get the child nodes of the metadata node
		NodeList ndLst = metadata.getChildNodes();
		NodeList txtNdList = null;

		// Then find the text node and get its children
		for (int i = 0; i < ndLst.getLength(); i++) {
			Node nde = ndLst.item(i);
			if (nde.getNodeType() == Node.ELEMENT_NODE) {
				String ndName = nde.getNodeName();
				switch (ndName) {
				case "Text":
					txtNdList = nde.getChildNodes();
					break;
				}

			}
		}

		// If we found the text node chlidrent, then process them
		if (txtNdList != null) {

			// We want to find the text entry node in the text node.
			// Then get the attribute map from it.
			NamedNodeMap map = null;
			for (int i = 0; i < txtNdList.getLength(); i++) {
				Node nde = txtNdList.item(i);
				if (nde.getNodeType() == Node.ELEMENT_NODE) {
					String ndName = nde.getNodeName();
					switch (ndName) {
					case "TextEntry":
						map = nde.getAttributes();
						break;
					}
				}
			}

			// If we found the map, then process it to get the
			// FITS header XML document.
			if (map != null) {
				String attribText = null;
				for (int i = 0; i < map.getLength(); i++) {
					Node attr = map.item(i);
					if (attr.getNodeType() == Node.ATTRIBUTE_NODE) {
						String ndName = attr.getNodeName();
						switch (ndName) {
						case "value":
							attribText = attr.getNodeValue();
							break;
						}
					}
				}

				// Parse the XML Fits header document if we found it.
				if (attribText != null) {
					try {
						DocumentBuilderFactory fctry = DocumentBuilderFactory.newInstance();
						DocumentBuilder bldr = fctry.newDocumentBuilder();
						try (InputStream is = new ByteArrayInputStream(attribText.trim().getBytes())) {
							doc = bldr.parse(is);
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}

			}
		}

		return doc;
	}

}
