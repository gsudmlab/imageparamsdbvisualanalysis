package edu.gsu.dmlab.stats;

public class FMeasure {

	int[] actuals;
	int[] predicteds;
	int[] classes;

	/**
	 * This method computes f1-score for each of the class labels and returns an
	 * array of f1-scores. This assumes that the class labels are relevant and
	 * furthermore, the array of classes is a consecutive sequence of integers
	 * starting from zero.
	 * 
	 * @param actuals
	 * @param predicteds
	 * @param classes
	 */
	public FMeasure(int[] actuals, int[] predicteds, int[] classes) {

		if (actuals.length != predicteds.length) {
			throw new IllegalArgumentException("The array of actual and apredicted classes must be of the same size.");
		}

		this.actuals = actuals;
		this.predicteds = predicteds;
		this.classes = classes;
	}

	/**
	 * This is the main method that computes the f1-Score based on the precision
	 * and recall calculated separately for each class.
	 * 
	 * @return an array of f1-scores, one measure for each class.
	 */
	public double[] measure() {

		int[][] confMat = this.computeConfusionMatrix();
		double precision = 0;
		double recall = 0;
		double[] f1Scores = new double[this.classes.length];

		// Calculate f1-Score for each class label
		for (int c : this.classes) {
			precision = this.computePrecision(confMat, c);
			recall = this.computeRecall(confMat, c);
			f1Scores[c] = this.computeF1Score(recall, precision);
		}

		return f1Scores;
	}

	/**
	 * Computes the confusion matrix for the multi-class predictions.
	 * 
	 * @return the confusion matrid: actuals X predicteds
	 */
	private int[][] computeConfusionMatrix() {

		int[][] matrix = new int[this.classes.length][this.classes.length];

		for (int i = 0; i < this.actuals.length; i++) {
			matrix[this.actuals[i]][this.predicteds[i]]++;
		}

		return matrix;
	}


	/**
	 * This method computes precision given that the class <code>c</code>
	 * is a positive prediction. That is, tp indicates the class
	 * <code>c</code> was predicted while <code>c</code> was the actual
	 * class. 
	 * <br>
	 * Formula: tp / (tp + fn)
	 * <br>
	 * 
	 * @param confMat oriented in a way that "actual" classes corresponds to
	 * the rows and the "predicted" classes correspond to the columns.
	 * @param c
	 * @return
	 */
	private double computeRecall(int[][] confMat, int c) {

		double tp = confMat[c][c];
		double rowSum = 0;

		for (int i = 0; i < confMat[0].length; i++) {
			rowSum += confMat[c][i];
		}
		return tp / rowSum;
	}

	/**
	 * This method computes precision given that the class <code>c</code>
	 * is a positive prediction. That is, tp indicates the class
	 * <code>c</code> was predicted while <code>c</code> was the actual
	 * class. 
	 * <br>
	 * Formula: tp / (tp + fp)
	 * 
	 * @param confMat oriented in a way that "actual" classes corresponds to
	 * the rows and the "predicted" classes correspond to the columns.
	 * @param c
	 * @return
	 */
	private double computePrecision(int[][] confMat, int c) {

		double tp = confMat[c][c];
		double colSum = 0;

		for (int i = 0; i < confMat.length; i++) {
			colSum += confMat[i][c];
		}
		return tp / colSum;
	}

	/**
	 * This computes the f1-score based on the given precision and recall.
	 * <br>
	 * Formula: 2 * ((precision * recall) / (precision + recall));
	 * @param recall
	 * @param precision
	 * @return
	 */
	private double computeF1Score(double recall, double precision) {
		
		return 2 * ((precision * recall) / (precision + recall));
	}

}
