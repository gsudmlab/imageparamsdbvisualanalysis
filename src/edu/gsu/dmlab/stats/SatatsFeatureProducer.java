package edu.gsu.dmlab.stats;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.common.math.Quantiles;
import com.google.common.math.Quantiles.Scale;

import edu.gsu.dmlab.database.interfaces.IImageParamDBConnection;
import edu.gsu.cs.dmlab.datatypes.ImageDBWaveParamPair;
import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;
import edu.gsu.dmlab.stats.interfaces.IFeatureValueProducer;
import smile.math.Math;
import smile.math.matrix.DenseMatrix;

public class SatatsFeatureProducer implements IFeatureValueProducer {

	IImageParamDBConnection dbconn;
	ImageDBWaveParamPair[] params;

	public SatatsFeatureProducer(IImageParamDBConnection dbconn, ImageDBWaveParamPair[] params) {
		this.dbconn = dbconn;
		this.params = params;
	}

	@Override
	public double[] getStats(IEvent ev) {
		List<double[]> statList = this.calculateStats(ev);
		double[] results = new double[this.params.length * 7];

		int count = 0;
		for (int i = 0; i < params.length; i++) {
			for (int j = 0; j < 7; j++) {
				results[count++] = statList.get(i)[j];
			}
		}

		return results;
	}

	private List<double[]> calculateStats(IEvent ev) {
		Scale s = Quantiles.quartiles();
		DenseMatrix[] paramVals = this.dbconn.getImageParamForEv(ev, params, false);

		ArrayList<double[]> results = new ArrayList<double[]>();
		for (DenseMatrix pV : paramVals) {

			ArrayList<Double> d = new ArrayList<Double>();
			double[][] arr = pV.array();
			double[] arr2 = new double[arr.length * arr[0].length];
			int count = 0;
			for (int i = 0; i < arr.length; i++) {
				for (int j = 0; j < arr[0].length; j++) {
					d.add(Double.valueOf(arr[i][j]));
					arr2[count++] = arr[i][j];
				}
			}
			Map<Integer, Double> m = s.indexes(1, 2, 3).compute(d);
			double maxVal = Math.max(arr);
			double minVal = Math.min(arr);
			double avg = Math.mean(arr2);
			double std = Math.sqr(Math.var(arr2));

			double[] ans = new double[7];
			ans[0] = minVal;
			ans[1] = m.get(1).doubleValue();
			ans[2] = m.get(2).doubleValue();
			ans[3] = m.get(3).doubleValue();
			ans[4] = maxVal;
			ans[5] = avg;
			ans[6] = std;
			results.add(ans);
		}
		return results;
	}

}
