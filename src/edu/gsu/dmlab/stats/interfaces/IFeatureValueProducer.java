package edu.gsu.dmlab.stats.interfaces;

import edu.gsu.cs.dmlab.datatypes.interfaces.IEvent;

public interface IFeatureValueProducer {
	
	public double[] getStats(IEvent ev);
}
